#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass beamer
\begin_preamble
\usetheme{Frankfurt}
% or ...

\setbeamercovered{transparent}
% or whatever (possibly just delete it)

\setbeamertemplate{navigation symbols}{}
\usepackage{pdfpcnotes}
%\setbeamertemplate{headline}{}

\usepackage{tikz}

\newcommand{\intset}{\mathbb{Z}}
\newcommand{\ff}[1]{\mathbb{F}_{#1}}

\usepackage{algorithm}
\usepackage{algpseudocode}


% Algorithms in French
\makeatletter 
\renewcommand{\ALG@name}{Algorithme}
\makeatother
\renewcommand{\algorithmicend}{\textbf{Fin}}
\renewcommand{\algorithmicif}{\textbf{Si}}
\renewcommand{\algorithmicthen}{\textbf{alors}}
\renewcommand{\algorithmicelse}{\textbf{Sinon}}
\renewcommand{\algorithmicfor}{\textbf{Pour}}
\renewcommand{\algorithmicforall}{\textbf{Pour tout}}
\renewcommand{\algorithmicdo}{\textbf{Faire}}
\renewcommand{\algorithmicwhile}{\textbf{Tant que}}
\renewcommand{\algorithmicreturn}{\textbf{Retourner}}
\renewcommand{\algorithmicfunction}{\textbf{Fonction}}
\algrenewcommand\algorithmicrequire{\textbf{Pre :}}
\algrenewcommand\algorithmicensure{\textbf{Post :}}	

\let\oldReturn\Return
\renewcommand{\Return}{\State\oldReturn}
\end_preamble
\use_default_options false
\maintain_unincluded_children false
\language french
\language_package default
\inputencoding auto
\fontencoding global
\font_roman times
\font_sans default
\font_typewriter default
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 2
\use_package amssymb 2
\use_package cancel 0
\use_package esint 0
\use_package mathdots 1
\use_package mathtools 0
\use_package mhchem 1
\use_package stackrel 0
\use_package stmaryrd 0
\use_package undertilde 0
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 2
\tocdepth 2
\paragraph_separation indent
\paragraph_indentation default
\quotes_language french
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
 Ce fichier est un exemple d'exposé:
\end_layout

\begin_layout Itemize
pour des conférences, 
\end_layout

\begin_layout Itemize
d'une durée approximative de 20 minutes, 
\end_layout

\begin_layout Itemize
avec un style ornemental.
\end_layout

\begin_layout Plain Layout
Pour afficher les explications, ouvrir les notes.
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
Copyright 2004 by Till Tantau <tantau@users.sourceforge.net>.
 
\end_layout

\begin_layout Plain Layout
Traduction de Philippe De Sousa <philippejjg@free.fr>.
 
\end_layout

\begin_layout Plain Layout
Adaptation à LyX par Jean-Pierre Chrétien <chretien@cert.fr> 
\end_layout

\begin_layout Plain Layout
En principe, ce fichier peut être redistribué et/ou modifié conformément
 aux termes de la GNU Public License, version 2.Cependant, ce fichier est
 un "exemple-type" qui peut être modifié selon vos propres besoins.
 Pour cette raison, si vous utilisez ce fichier en tant qu'"exemple-type"
 et non spécifiquement pour le distribuer en tant que partie d'un package
 ou programme, je vous donne la permission exceptionnelle de copier librement
 et de modifier ce fichier et même d'effacer ce message de copyright.
 
\end_layout

\begin_layout Plain Layout
Correction mineure (style Institute au lieu de style Date) des fichiers
 originaux le 12 jan 07 par Jean-Pierre Chrétien <chretien@cert.fr> 
\end_layout

\end_inset


\end_layout

\begin_layout Title
PGCD de polynômes multivariés dans le logiciel TRIP
\end_layout

\begin_layout Author
Noé
\begin_inset space ~
\end_inset

Brucy
\begin_inset Flex InstituteMark
status open

\begin_layout Plain Layout
1
\end_layout

\end_inset

 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash

\backslash

\end_layout

\end_inset

 
\size small
sous la supervision de
\size default
 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash

\backslash

\end_layout

\end_inset

 Jacques
\begin_inset space ~
\end_inset

Laskar
\begin_inset Flex InstituteMark
status collapsed

\begin_layout Plain Layout
2
\end_layout

\end_inset

 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
and
\end_layout

\end_inset

 Mickaël Gastineau
\begin_inset Flex InstituteMark
status collapsed

\begin_layout Plain Layout
2
\end_layout

\end_inset


\begin_inset Note Note
status collapsed

\begin_layout Itemize
composer les noms dans l'ordre dans lequel ils apparaîtrons dans l'article;
 
\end_layout

\begin_layout Itemize
utiliser l'insert 
\begin_inset Quotes fld
\end_inset

MarqueInstitution
\begin_inset Quotes frd
\end_inset

 (
\family typewriter
Insérer->Inserts personnalisables->MarqueInstitution
\family default
) niquement si les auteurs ont des affiliations  différentes.
\end_layout

\end_inset


\begin_inset Argument 1
status open

\begin_layout Plain Layout
Noé Brucy
\begin_inset Note Note
status collapsed

\begin_layout Itemize
facultatif, à utiliser seulement avec plusieurs auteurs;
\end_layout

\begin_layout Itemize
s'il y a vraiment beaucoup d'auteurs, utiliser 
\begin_inset Quotes eld
\end_inset

Auteur et al.
\begin_inset Quotes erd
\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Institute
\begin_inset Flex InstituteMark
status collapsed

\begin_layout Plain Layout
1
\end_layout

\end_inset

Département  d'Informatique
\begin_inset Newline newline
\end_inset

ENS Rennes
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
and
\end_layout

\end_inset

 
\begin_inset Flex InstituteMark
status collapsed

\begin_layout Plain Layout
2
\end_layout

\end_inset

Équipe ASD, IMMCE, Observatoire de Paris
\begin_inset Newline newline
\end_inset


\end_layout

\begin_layout Date
ENS Rennes Stage DIT L3 2016
\begin_inset Note Note
status collapsed

\begin_layout Itemize
utilisez le nom de la conférence ou son abréviation; 
\end_layout

\begin_layout Itemize
n'a pas réellement d'importance pour l'assistance qui sera présente lors
 de la conférence; mais en a pour les personnes (y compris vous-même) qui
 liront les transparents en ligne.
\end_layout

\end_inset


\begin_inset Argument 1
status open

\begin_layout Plain Layout
2016
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
facultatif, devrait être une abréviation du nom de la conférence
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
Si vous avez un fichier nommé "université-logo-nomfichier.xxx", où xxxest
 un format graphique accepté par LyX (comme par exemple .png), alors vous
 pouvez insérer votre logo ainsi :
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status collapsed

\begin_layout Plain Layout

%
\backslash
logo{
\backslash
includegraphics[height=0.5cm]{institution-logo-filename}}
\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
Avec la syntaxe pgf, construction équivalente, mais format graphique compris
 par latex ou pdflatex:
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout

%
\backslash
pgfdeclareimage[height=0.5cm]{institution-logo}{institution-logo-filename}
\end_layout

\begin_layout Plain Layout

%
\backslash
logo{
\backslash
pgfuseimage{institution-logo}} 
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
À supprimer si vous ne voulez pas que la table des matières apparaisse au
 début de chaque sous-section :
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
AtBeginSection[]{%
\end_layout

\begin_layout Plain Layout

  
\backslash
frame<beamer>{ 
\end_layout

\begin_layout Plain Layout

    
\backslash
frametitle{Lignes directrices}   
\end_layout

\begin_layout Plain Layout

    
\backslash
tableofcontents[currentsection] 
\end_layout

\begin_layout Plain Layout

% Vous pouvez, si vous le souhaitez ajouter l'option [pausesections]
\end_layout

\begin_layout Plain Layout

  }
\end_layout

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
Si vous souhaitez recouvrir vos transparents un à un, utilisez la commande
 suivante (pour plus d'info, voir la page 74 du manuel d'utilisation de
 Beamer (version 3.06) par Till Tantau) :
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout

%
\backslash
beamerdefaultoverlayspecification{<+->}
\end_layout

\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Sommaire
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset CommandInset toc
LatexCommand tableofcontents

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
Structurer l'exposé est une tâche difficile et la structure suivante pourrait
 ne pas convenir.
 Voici quelques règles à appliquer pour cet exemple ci :
\end_layout

\begin_layout Itemize
avoir exactement deux ou trois sections (autre que le sommaire); 
\end_layout

\begin_layout Itemize
tout au plus trois sous-sections par section; 
\end_layout

\begin_layout Itemize
parler approximativement entre 30 secondes et 2 minutes par transparent.
 Il  devrait donc y avoir entre 15 et 30 transparents, pour tout dire;
\end_layout

\begin_layout Itemize
le public d'une conférence connaîtra probablement peu de chose sur le sujet
  que vous serez en train de traiter.
 Donc, *simplifier*
\begin_inset space ~
\end_inset

!
\end_layout

\begin_layout Itemize
dans un exposé de 20 minutes, garder à l'esprit les idées principales est
 largement  suffisant pour votre assistance.
 Laissez tomber certains détails, même s'ils vous  semblent nécessaires;
 
\end_layout

\begin_layout Itemize
si un détail qui est vital pour une preuve/mise en oeuvre est omis,  le
 dire simplement une fois.
 Ce sera suffisant pour l'auditoire.
 
\end_layout

\end_inset


\end_layout

\end_deeper
\begin_layout Section
Contexte et motivation
\end_layout

\begin_layout Subsection
Contexte : l'équipe ASD
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
L'équipe ASD
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
À l'observatoire de Paris.
\end_layout

\begin_deeper
\begin_layout Pause

\end_layout

\end_deeper
\begin_layout Itemize
IMCCE : Institut de Mécaniques Celeste et de calcul des éphémérides.
\end_layout

\begin_deeper
\begin_layout Pause

\end_layout

\end_deeper
\begin_layout Itemize
ASD : Astronomie et systèmes dynamiques
\end_layout

\begin_deeper
\begin_layout Pause

\end_layout

\begin_layout Itemize
Spécialisée dans le calcul des éphémériques, la planétologie et la mécanique
 celeste.
\end_layout

\begin_layout Pause

\end_layout

\begin_layout Itemize
Doit manipuler des séries de pertubations pour approcher les solutions du
 problème des N corps.
\end_layout

\end_deeper
\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Le logiciel TRIP 
\end_layout

\end_inset


\end_layout

\begin_layout Frame

\end_layout

\begin_deeper
\begin_layout Itemize
La manipulation de séries ne peut se faire manuellement.
\end_layout

\begin_layout Pause

\end_layout

\begin_layout Itemize
Jeu de routines écrites par Jacques Laskar pour assiter la manipulation
 de séries.
\end_layout

\begin_deeper
\begin_layout Pause

\end_layout

\end_deeper
\begin_layout Itemize
Transformé avec l'aide de Mickaël Gastineau en un logiciel de calcul formel
 et numérique, spécialisé dans la manipulation de séries tronquées (polynômes)
\end_layout

\end_deeper
\begin_layout Subsection
Calcul de PGCD en calcul formel 
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Plus grand diviseur commun de deux polynômes
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Example
\begin_inset Argument 2
status open

\begin_layout Plain Layout
Polynôme multivarié à coefficients entiers
\end_layout

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
[ P = 2x_1^7 x_2 + x_1^6 x_2 ^2 + 4 x_1 ^4 + 2 x_1 ^2 x_2 
\backslash
in 
\backslash
mathbb{Z}[x_1, x_2] 
\backslash
]
\end_layout

\end_inset


\end_layout

\begin_layout Pause

\end_layout

\begin_layout Example
\begin_inset Argument 2
status open

\begin_layout Plain Layout
Divisibilité
\end_layout

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
[ P = (2x_1 ^ 2 + x_1 x_2  ) (x_1 ^5 x_2 + 2x_1) 
\backslash
]
\end_layout

\begin_layout Plain Layout

$G = (2x_1 ^ 2 + x_1 x_2  )$ divise P
\end_layout

\end_inset


\end_layout

\begin_layout Pause

\end_layout

\begin_layout Example
\begin_inset Argument 2
status open

\begin_layout Plain Layout
PGCD
\end_layout

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout

$G$ est le PGCD de $P$ et de $Q = 4 x_2 (2x_1 ^ 2 + x_1 x_2) $
\end_layout

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Pourquoi calculer le PGCD ?
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Pour la simplification des expressions rationnelles
\end_layout

\begin_layout Example

\end_layout

\begin_layout Example
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
[ 
\backslash
dfrac{P}{Q} = 
\backslash
dfrac{2x_1^7 x_2 + x_1^6 x_2 ^2 + 4 x_1 ^4 + 2 x_1 ^2 x_2}{ 8 x_1 ^ 2 x_2
  + 4 x_1 x_2 ^2 } = 
\backslash
dfrac{x_1 ^5 x_2 + 2x_1}{4 x_2} 
\backslash
]
\end_layout

\end_inset


\end_layout

\begin_layout Pause

\end_layout

\begin_layout Itemize
Pour la factorisation de polynômes
\end_layout

\begin_layout Pause

\end_layout

\begin_layout AlertBlock
\begin_inset Argument 2
status open

\begin_layout Plain Layout
Bilan
\end_layout

\end_inset


\end_layout

\begin_layout AlertBlock
Le calcul PGCD est un élement central de tout système de calcul formel manipulan
t des polynômes
\end_layout

\end_deeper
\begin_layout Subsection
Ma contribution 
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Comment TRIP calcule un PGCD
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
Actuellement, TRIP utilise Maple pour calculer le PGCD.
\end_layout

\begin_layout Standard
\align center
\begin_inset Graphics
	filename Schémas/Calcul_PGCD_TRIP.pdf
	height 7.5cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Comment TRIP calcule un PGCD
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
Problème : temps de calcul et complétude de TRIP
\end_layout

\end_deeper
\begin_layout Frame

\end_layout

\begin_deeper
\begin_layout Standard
\align center
\begin_inset Graphics
	filename Schémas/Calcul_PGCD_TRIP_cons.pdf
	height 7.5cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Objectifs du stage
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Block
\begin_inset Argument 2
status open

\begin_layout Plain Layout
Ma mission
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Chercher dans la littérature les algorithmes de calcul de PGCD
\end_layout

\begin_layout Itemize
Analyser
\end_layout

\begin_deeper
\begin_layout Itemize
Leur efficacité, leurs point forts et les cas pathologiques
\end_layout

\begin_layout Itemize
Les primitives nécéssaires à leur implémentation
\end_layout

\end_deeper
\begin_layout Itemize
Coder les plus prometteurs dans le langage TRIP
\end_layout

\begin_layout Itemize
Comparer les performances réelles et mesurer le gain obtenu
\end_layout

\end_deeper
\end_deeper
\begin_layout Section
Algorithmes de calcul du PGCD
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Algorithmes de calcul du PGCD
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\align center
\begin_inset Graphics
	filename Schémas/Algoos.pdf
	height 8.3cm

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Subsection
Pourquoi pas Euclide ?
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
L'algorithme d'Euclide
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\align center
\begin_inset ERT
status open

\begin_layout Plain Layout

    
\backslash
begin{algorithmic}[1]      
\backslash
Require{$A, B$ dans $E$ un anneau euclidien}      
\backslash
Ensure{Le PGCD de $A$ et $B$, à un inversible de E près.}      
\backslash
Function{PGCD}{$A, B$}        
\backslash
State{$r_0 := A$, $r_1 := B$, $i := 1$}        
\backslash
While{$r_i 
\backslash
neq 0$}          
\backslash
State{Écrire $r_{i - 1} = q_i  r_i + r_{i + 1}$ la division euclidienne
 de $r_{i - 1}$ par $r_i$}          
\backslash
State{$i := i + 1$}        
\backslash
EndWhile         
\backslash
Return{$r_{i - 1}$}           
\backslash
EndFunction   
\backslash
end{algorithmic} 
\end_layout

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Il faut adapter Euclide
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Algorithme d'Euclide non utlisable tel quel : la division euclidienne n'est
 pas toujours possible.
\end_layout

\begin_layout Example
\begin_inset ERT
status open

\begin_layout Plain Layout

$A(x_1, x_2) = x_1^2 + x_2$
\end_layout

\end_inset

 et
\begin_inset ERT
status open

\begin_layout Plain Layout

 $B(x_1, x_2) = x_1 x_2$
\end_layout

\end_inset

 n'admettent pas de division euclidienne.
\end_layout

\begin_layout Pause

\end_layout

\begin_layout ExampleBlock
\begin_inset Argument 2
status open

\begin_layout Plain Layout
Solution
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Polynôme multivarié = polynôme univarié à coefficients polynomiaux
\end_layout

\begin_layout Itemize
Multiplier le dividende par une constante pour rendre la division possible
\end_layout

\end_deeper
\begin_layout Pause

\end_layout

\begin_layout Example
\begin_inset ERT
status open

\begin_layout Plain Layout

$x_2A(x_1) = x_1 B(x_1) + x_2^2$
\end_layout

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Un exemple : ça explose !
\begin_inset Foot
status open

\begin_layout Plain Layout
D'apres Knuth, 1969
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
On cherche le PGCD avec l'algorithme d'Euclide modifié de : 
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
begin{align*} A(x) &= x^8 + x^6 - 3 x^4 + 8 x^2 + 2x - 5 
\backslash

\backslash
 
\end_layout

\begin_layout Plain Layout

 B(x) &= 3x^6 + 5x^4 - 4x^2 - 9x + 21 
\backslash
end{align*}
\end_layout

\end_inset


\end_layout

\begin_layout Overprint
\begin_inset Argument item:1
status open

\begin_layout Plain Layout
2
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
On obtient la suite de restes : 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
begin{align*} R_2(x) &= -15x^4 + 3x^2 - 9 
\backslash

\backslash

\end_layout

\begin_layout Plain Layout

  R_3(x) &= 15795x^2 +30375x - 59535 
\backslash

\backslash

\end_layout

\begin_layout Plain Layout

  R_4(x) &= 1254542875143750x - 1654608338437500 
\backslash

\backslash

\end_layout

\begin_layout Plain Layout

R_5(x) &= 12593338795500743100931141992187500 
\backslash
end{align*}
\end_layout

\end_inset


\end_layout

\end_deeper
\begin_layout Overprint
\begin_inset Argument item:1
status open

\begin_layout Plain Layout
3
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout AlertBlock
\begin_inset Argument 2
status open

\begin_layout Plain Layout
Problème
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Croissance exponentielle de la taille des coefficients des résultats intermédiai
res
\end_layout

\end_deeper
\begin_layout ExampleBlock
\begin_inset Argument 2
status open

\begin_layout Plain Layout
Solution
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Les algorithmes PRS (
\emph on
Polynomial Remainder Sequence
\emph default
) sont des améliorations de cet algorithme qui limitent cette croissance.
\end_layout

\end_deeper
\end_deeper
\end_deeper
\begin_layout Subsection
Algorithmes modulaires
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
MGCD
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Algo modulaire : 
\end_layout

\begin_deeper
\begin_layout Itemize
le problème est réduit au cas univarié dans un corps fini.
\end_layout

\begin_layout Itemize
plusieurs images sont calculées, et la solution est reconstruite via le
 théorème chinois
\end_layout

\end_deeper
\begin_layout Overprint
\begin_inset Argument item:1
status open

\begin_layout Plain Layout
1
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\align center
\begin_inset Graphics
	filename Schémas/tikz/MGCD.pdf
	height 4.5cm
	BoundingBox 0bp 0bp 372bp 198bp

\end_inset


\end_layout

\end_deeper
\begin_layout Overprint
\begin_inset Argument item:1
status open

\begin_layout Plain Layout
2
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Variantes pour polynômes creux :
\end_layout

\begin_deeper
\begin_layout Itemize
SparseMod
\end_layout

\begin_layout Itemize
EZ-GCD
\end_layout

\end_deeper
\end_deeper
\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Subsection
Algorithme Heuristique
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
L'idée
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Algo heuristique : 
\end_layout

\begin_deeper
\begin_layout Itemize
un solveur (évalue les polynômes, calcul un PGCD entier puis reconstruit
 la solution)
\end_layout

\begin_layout Itemize
un vérificateur (test de division)
\end_layout

\end_deeper
\begin_layout Standard
\align center
\begin_inset Graphics
	filename Schémas/tikz/GCDHEU.pdf
	height 5cm
	BoundingBox 0bp 0bp 372bp 198bp

\end_inset


\end_layout

\end_deeper
\begin_layout Separator

\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Un exemple
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Overprint
\begin_inset Argument item:1
status open

\begin_layout Plain Layout
1
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
begin{align*} A(x) &= x + x^3
\backslash

\backslash
 B(x) &= 4 + x + 4x^2 + x^3
\backslash

\backslash
 
\backslash
end{align*}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
On considère deux polynômes
 
\lang french

\begin_inset ERT
status open

\begin_layout Plain Layout

$A$
\end_layout

\end_inset

 et 
\begin_inset ERT
status open

\begin_layout Plain Layout

$B$
\end_layout

\end_inset

.
\end_layout

\end_deeper
\begin_layout Overprint
\begin_inset Argument item:1
status open

\begin_layout Plain Layout
2
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
begin{align*} A(4) &= 68
\backslash

\backslash
 B(4) &= 136
\backslash

\backslash
 
\backslash
end{align*}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
On choisit un point d'évaluation 
\begin_inset ERT
status open

\begin_layout Plain Layout

$
\backslash
xi$
\end_layout

\end_inset

.
 Ici, 
\begin_inset ERT
status open

\begin_layout Plain Layout

$
\backslash
xi = 4$
\end_layout

\end_inset

 est la plus petite valeur possible.
\end_layout

\end_deeper
\begin_layout Overprint
\begin_inset Argument item:1
status open

\begin_layout Plain Layout
3
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
begin{align*} 
\end_layout

\begin_layout Plain Layout


\backslash
gamma_4 &:= PGCD(A(4), B(4)) = 68
\end_layout

\begin_layout Plain Layout


\backslash
end{align*}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
On calcule le PGCD 
\begin_inset ERT
status open

\begin_layout Plain Layout

$
\backslash
gamma$ 
\end_layout

\end_inset

dans 
\begin_inset ERT
status open

\begin_layout Plain Layout

$
\backslash
intset$
\end_layout

\end_inset


\end_layout

\end_deeper
\begin_layout Overprint
\begin_inset Argument item:1
status open

\begin_layout Plain Layout
4
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
begin{align*} 
\end_layout

\begin_layout Plain Layout


\backslash
gamma_4 &= 1 
\backslash
cdot 4 + 1 
\backslash
cdot 4^3 
\backslash

\backslash

\end_layout

\begin_layout Plain Layout


\backslash
end{align*}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
On décompose
\begin_inset ERT
status open

\begin_layout Plain Layout

 $
\backslash
gamma$
\end_layout

\end_inset

 en base 
\begin_inset ERT
status open

\begin_layout Plain Layout

$
\backslash
xi$
\end_layout

\end_inset

 symétriquement : 
\begin_inset ERT
status open

\begin_layout Plain Layout

$
\backslash
gamma = a_0 + a_1
\backslash
xi + a_2
\backslash
xi^2 +~
\backslash
dots$
\end_layout

\end_inset

, avec 
\begin_inset ERT
status open

\begin_layout Plain Layout

$a_i$ 
\end_layout

\end_inset

compris entre 
\begin_inset ERT
status open

\begin_layout Plain Layout

$
\backslash
lceil 
\backslash
frac{
\backslash
xi}{2}
\backslash
rceil $
\end_layout

\end_inset

et 
\begin_inset ERT
status open

\begin_layout Plain Layout

$
\backslash
lfloor 
\backslash
frac{
\backslash
xi}{2} 
\backslash
rfloor$
\end_layout

\end_inset

 pour tout 
\begin_inset ERT
status open

\begin_layout Plain Layout

$i$
\end_layout

\end_inset

.
\end_layout

\end_deeper
\begin_layout Overprint
\begin_inset Argument item:1
status open

\begin_layout Plain Layout
5
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
begin{align*}  G_4 &= x + x^3 
\backslash
end{align*}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
On propose le PGCD 
\begin_inset ERT
status open

\begin_layout Plain Layout

$G_4$
\end_layout

\end_inset

.
\end_layout

\begin_layout Standard
Le vérificateur teste si 
\begin_inset ERT
status open

\begin_layout Plain Layout

$G_4$
\end_layout

\end_inset

 divise 
\begin_inset ERT
status open

\begin_layout Plain Layout

$A$ 
\end_layout

\end_inset

et 
\begin_inset ERT
status open

\begin_layout Plain Layout

$B$
\end_layout

\end_inset

.
 Ce n'est pas le cas, donc un autre point d'évaluation est choisi.
\end_layout

\end_deeper
\begin_layout Overprint
\begin_inset Argument item:1
status open

\begin_layout Plain Layout
6
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
begin{align*}  G_{10} &= 1 + x^2   
\backslash
end{align*}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Pour éviter un autre echec, on évite les multiples de 
\begin_inset ERT
status open

\begin_layout Plain Layout

$4$
\end_layout

\end_inset

.
 On prend 
\begin_inset ERT
status open

\begin_layout Plain Layout

$
\backslash
xi = 10$
\end_layout

\end_inset

.
 
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout

$G_{10}$
\end_layout

\end_inset

 divise 
\begin_inset ERT
status open

\begin_layout Plain Layout

$A$
\end_layout

\end_inset

 et 
\begin_inset ERT
status open

\begin_layout Plain Layout

$B$
\end_layout

\end_inset

.
\end_layout

\begin_layout Standard
Le choix de 
\begin_inset ERT
status open

\begin_layout Plain Layout

$
\backslash
xi$
\end_layout

\end_inset

 est tel que cela assure que 
\begin_inset ERT
status open

\begin_layout Plain Layout

$G_{10} = PGCD(A, B)$
\end_layout

\end_inset


\end_layout

\end_deeper
\end_deeper
\begin_layout Section
Résultats obtenus
\end_layout

\begin_layout Subsection
Polynômes denses
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Polynômes denses
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Overprint
\begin_inset Argument item:1
status open

\begin_layout Plain Layout
1
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename /home/noe/Noe/Travail/L3/01-Informatique/08-XTRA/02-Stage/StageIMCCE/TRIP_GCD/Rapport/Bench/v2dg0-25dc-1t0s16.pdf
	height 6cm

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Polynômes denses bivariés (degré 2n)
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_deeper
\begin_layout Overprint
\begin_inset Argument item:1
status open

\begin_layout Plain Layout
2
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename /home/noe/Noe/Travail/L3/01-Informatique/08-XTRA/02-Stage/StageIMCCE/TRIP_GCD/Rapport/Bench/v3dg0-20dc-1t0s16.pdf
	height 6cm

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Polynômes denses trivariés (degré 2n)
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_deeper
\begin_layout Overprint
\begin_inset Argument item:1
status open

\begin_layout Plain Layout
3
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename /home/noe/Noe/Travail/L3/01-Informatique/08-XTRA/02-Stage/StageIMCCE/TRIP_GCD/Rapport/Bench/grid_xdg0-30_ratio_GCDHEUoverMaple+Transfert_v2dg30dc0-30t0s16.pdf
	height 6cm

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Polynômes denses trivariés (degré 2n)
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_deeper
\end_deeper
\begin_layout Subsection
Polynômes creux
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Polynômes creux
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Overprint
\begin_inset Argument item:1
status open

\begin_layout Plain Layout
1
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename /home/noe/Noe/Travail/L3/01-Informatique/08-XTRA/02-Stage/StageIMCCE/TRIP_GCD/Rapport/Bench/v1-20dg10dc10t20s16.pdf
	height 6cm

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Polynômes creux (400 termes, degré 40)
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_deeper
\begin_layout Overprint
\begin_inset Argument item:1
status open

\begin_layout Plain Layout
2
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename /home/noe/Noe/Travail/L3/01-Informatique/08-XTRA/02-Stage/StageIMCCE/TRIP_GCD/Rapport/Bench/grid_xv1-15_ratio_Maple+transfertoverSparseMod_v15dg0-15dc-1t20s16.pdf
	height 6cm

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Polynômes creux (400 termes, degré 2n)
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_deeper
\end_deeper
\begin_layout Section*
Résumé
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Résumé
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
J'ai
\color none
 
\color inherit

\begin_inset Flex Alert
status open

\begin_layout Plain Layout
étudié, compris et analysé
\end_layout

\end_inset


\color none
 
\emph on
\color inherit
5
\emph default
\color none
 algorithmes et leur variantes
\end_layout

\begin_layout Itemize
Je les ai
\color none
 
\color inherit

\begin_inset Flex Alert
status open

\begin_layout Plain Layout
implémentés et testés
\end_layout

\end_inset


\color none
 dans le language TRIP pour un total d'environ 
\emph on
\color inherit
3200 
\emph default
lignes de code.
\end_layout

\begin_layout Itemize
J'ai
\color none
 
\color inherit

\begin_inset Flex Alert
status open

\begin_layout Plain Layout
analysé leur performances
\end_layout

\end_inset


\color none
 et en ai déduit quels algorithmes utiliser et dans quels cas.
\end_layout

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
  Les perspectives sont toujours facultatives.
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Itemize
Améliorations possibles
\end_layout

\begin_deeper
\begin_layout Itemize
Coder les algorithmes directement dans le noyau de TRIP, en C++.
\end_layout

\begin_layout Itemize
Coder EEZ-GCD, une amélioration de EZ-GCD.
\end_layout

\begin_layout Itemize
Utiliser la parallélisation.
\end_layout

\end_deeper
\end_deeper
\end_body
\end_document
