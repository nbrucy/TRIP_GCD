

macro plot_data[title]
{
  title_data = title + "_with_err.dat"$

  plotps title +  ".pdf";
  gnuplot;
    set term pdf color
    set xlabel 'n'
    set ylabel "temps réel (s)"
    set ytics format "10^{%L}"
    set key outside below
    set logscale y
    set grid
    
    set style line 1 lc rgb 'blue' lt 1 lw 1 pt 1  ps 0.1
    set style line 2 lc rgb 'red' lt 1 lw 1 pt 1  ps 0.1
    set style line 3 lc rgb 'green' lt 1 lw 1 pt 1  ps 0.1
    set style line 4 lc rgb 'brown' lt 1 lw 1 pt 1  ps 0.1

  end;

  gnuplot;
    plot '@title_data@' using 1:2 title 'GCDHEU' with linespoints ls 1,\
         '@title_data@' using 1:4 title 'MGCD' with linespoints ls 2,\
         '@title_data@' using 1:6 title 'Maple' with linespoints ls 3,\
         '@title_data@' using 1:8 title 'Maple (avec transfert)' with linespoints ls 4,\
         '@title_data@' using 1:2:3 notitle  w errorbars ls 1,\
         '@title_data@' using 1:4:5 notitle  w errorbars ls 2,\
         '@title_data@' using 1:6:7 notitle  w errorbars ls 3,\
         '@title_data@' using 1:8:9 notitle  w errorbars ls 4,\
    ;
        
    end;
  plotps_end;

}$
