
macro plot_grid[title_grid] 
{
  private _ALL$
  title_data = title_grid + ".dat" $
  
  plotps title_grid + ".pdf";
  gnuplot;
  set term  pdf color
  end;
  
  gnuplot;
  set cbrange[0:20];
  set palette defined (0 'red', 1 'orange', 1.1 'yellow', 10  'green', 20 'blue')  
  set xlabel 'nombre de variables'
  set ylabel 'n'
  set ylabel offset 2,0,0
  set xtics 1,1,15
  set ytics 0,1,15
  set ylabel rotate by 0

  set title 'temps(Maple avec transfert)/temps(SparseMod)'
  set cbtics add ( " 1" 1)
  plot '@title_data@' matrix using ($1 + 1):2:3 with image notitle ;
  end;
  plotps_end;
};
