/** 
 * Test cases to GCD computation of multivariate polynomials over integers.
 * 
 * May 2016
 * 
 * Noé Brucy, ENS Rennes,
 * Intern at IMCCE, team ASD
 * Under supervision of Jacques Laskar and Mickael Gastineau
 */




/** My test cases */

o = x + y + x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + X_1 + X_2 + X_3 + X_4 + X_5$

m_a1 = (x - 1)*(x - 2)*(x - 3) $
m_b1 = (x - 1)*x $

m_a2 = (x - 1)*(x - 2)*x  $
m_b2 = (x - 2)*(x - 3)*(x - 4) $

m_a3 = x*(x - 1)*(x - 2)*(x - 3)  $
m_b3 = (x - 3)*(x - 4)*(x - 5)*(x - 6)*(x - 7) $

m_d4 = x - 9$
m_f4 = x^3 - 3*x + 2 $
m_g4 = 2*x^2 - 1$
m_a4 = m_d4 * m_f4 $
m_b4 = m_d4 * m_g4 $

m_a8 = 54
 - 225*x_1
 - 315*x_1*x_2
 + 225*x_1**2
 + 147*x_2
 + 98*x_2**2
$

m_b8 =  - 18
   + 117*x_1
 + 294*x_1*x_2
 - 180*x_1**2
 - 105*x_2
 - 98*x_2**2
$



m_a9  = 
   78189804
 - 185703*x_2
 + 98*x_2**2
 $

m_b9 = 
 - 62588988
 + 173355*x_2
 - 98*x_2**2
 $
 
m_f10 = 4 + x1^2 + x2^2$
m_g10 = 2 + x1 + x2 $
m_d10 = 3*x1*x2 + x1 $

m_f11 = 4 + x1^2 + x2^2 + x3^8$
m_g11 = 2 + x1 + x2*x3^4 + x3 $
m_d11 = 3*x1*x2 + x1 + 3*x1*x3  $

m_d12 = x1*x2^2 - 5*x2$
m_f12 = x1*x2 + 5*x1 $
m_g12 = x1 + 1$

m_d13 = (4*y^2+2*z)*x^2 + (10*y^2+6*z) $
m_f13 = 4*x +z*x^2 + z^4*y*x^3 $
m_g13 = 3*x*y*z + 9*z^4*y*x^2 + 45*z^2*y^5*x^3 $

m_a13 = m_d13 * m_f13$
m_b13 = m_d13 * m_g13$

m_d14 = (y + 50)*x^3 + 100*y$
m_a14 = (x - y + 1)*m_d14 $
m_b14 = (x + y + 1)*m_d14 $

m_d15 = (3*y^2 - 90)*x^3 + 12*y + 100 $
m_a15 = (x - y + 1)*m_d15 $
m_b15 = (x + y + 1)*m_d15 $


m_vars = coef_tabvar(x +  y + z) $

m_d16 = (y + 2)*x^3 + 12*y^2 + 24*y$
m_a16 = (x - y + 1)*m_d16 $
m_b16 = (x + y + 1)*m_d16 $

m_u17 = (4*y*x^2 + 26*z*x ) $
m_w17 = (3*x^2 +y^2 ) $
m_a17 = m_u17*m_w17 $


m_b17 = [1 : 0] $

m_u17I = coef_num(Mod(m_u17, 5), (m_vars[2:], m_b17)) $ 
m_w17I = coef_num(Mod(m_w17, 5), (m_vars[2:], m_b17)) $

m_gamma17 = 4*y $
m_lca17 = 12*y $

m_u18 = (x^3 + 4*y*x^2 + 36*z*x + 3*y + 1) $
m_w18 = (x^3 + 6*x^2 +y^2*z + 2*z + 1) $
m_a18 = m_u18*m_w18 $

m_u18I = Mod(x^3 + 1, 5) $ 
m_w18I = Mod(x^3 + x^2 + 1, 5) $

m_gamma18 = 1 $
m_lca18 = 1 $

m_b18 = [0: 0] $


m_u19 =  y*x^2  $
m_w19 =  x^2 + 1 $
m_a19 = m_u19*m_w19 $


m_b19 = [1] $

m_u19I = coef_num(Mod(m_u19, 5), (m_vars[2:2], m_b19)) $ 
m_w19I = coef_num(Mod(m_w19, 5), (m_vars[2:2], m_b19)) $

m_gamma19 = y $
m_lca19 = y $


m_a20 = x^8 + (y + y^5)*x^6 + 3*(y^2 + 1)*x^4 + y*x^3 + 4*y$ 
m_b20 = 3*y*x^6 + y^2*x^2 + (5*y + 3)*x + 7$ 


m_a21 = x^4  + 4*x^2*y + 12*y + 3$ 
m_b21 = (3 + y^2)*x^3 + (5*y + 3)*x + 7$ 

m_a22 = 
   3
 + 1*y**2
$
m_b22 = 
   27
 + 108*y
 + 18*y**2
 + 72*y**3
 + 3*y**4
 + 12*y**5
$
  

m_a23  = 1*x3**2*x6
 + 1*x3**4*x5 $

m_b23 = 
   1*x3*x6**2
 + 1*x3**2*x4**2*x5**2
 + 1*x3**2*x4**2*x6*x5**4
 + 1*x3**3*x6*x5 $



/** Zippel Tests cases (See Zippel1979)*/

z_d1 = x1^2 + x1 +3$

z_f1 = 2*x1^2 + 2*x1 + 1$
z_g1 = x1^2 + 2*x1 + 2$

z_a1 = z_d1 * z_f1$
z_b1 = z_d1 * z_g1$


z_d2 = 2*x1^2*x2^2 + x1*x2 + 2*x1$

z_f2 = x2^2 + 2*x1^2*x2 + x1^2 + 1$
z_g2 = x1^2*x2^2 + x1^2*x2 + x1*x2 + x1^2 + x1$

z_a2 = z_d2 * z_f2$
z_b2 = z_d2 * z_g2$

z_a2mod = Mod(z_a2, 23) $
z_b2mod = Mod(z_b2, 23) $

z_f2_2 = 
   5
 + 8*x2
 + 1*x2**2
$

z_g2_2 = 
   6
 + 6*x2
 + 4*x2**2
$

z_d2_2 = 
   4
 + 2*x2
 + 8*x2**2
$

z_d6 = x1*x2^2*x4^2*x6^2 + x1*x2^2*x3^2*x4*x5^2*x6^2 + x1^2*x3*x6^2 + x1^2*x2*x3^2*x4*x5^2*x6 +x1^2*x2*x3^2*x4 $
z_f6 = x1^2*x2*x4*x5^2*x6^2 + x1*x3*x5^2*x6^2 + x1^2*x2^2*x6^2 + x1^2*x2^2*x3^2*x5*x6 +x1*x3^2*x4*x5 $
z_g6 = x2^2*x3^2*x4*x5^2*x6 + x1*x4^2*x5*x6 + x2^2*x3^2*x4*x5*x6 + x1*x2^2*x3*x4^2*x6 +x1^2*x3*x5^2 $

z_a6 = z_d6 * z_f6 $
z_b6 = z_d6 * z_g6 $


/** Geddes exercices and exemples (See Geddes1992, chapter 6)*/

gex6_a5 = x^3 + 10*x^2 -432*x +5040 $
gex6_u5 = Mod(x, 5) $
gex6_w5 = Mod(x**2 - 2, 5) $


gex6_a7 = 12*x^3 + 10*x^2 -36*x + 35 $
gex6_u7 = Mod(2*x, 5) $
gex6_w7 = Mod(x**2 + 2, 5) $

gex6_a7m = (2*x + 5*y)*(6*x^2 - 2*(y + 4)*x + 10*y - 3) $
gex6_u7m = 2*x + 5 $
gex6_w7m = 6*x^2 - 10*x + 7$
gex6_Jm = [0 : 1] $
gex6_lc1m = 2 $
gex6_lc2m = 6 $

gex6_a7m2 = (2*x + z^2 + 5*y -1)*(3*(z+1)*x^2 - 2*(y + 4)*x + 10*y - 3*z^2 + z*y - 1) $
gex6_u7m2 = 2*x + 5 $
gex6_w7m2 = 6*x^2 - 10*x + 7$
gex6_Jm2 = [0 : 1 : 1] $
gex6_lc1m2 = 2 $
gex6_lc2m2 = 3*(z + 1) $
 

/** Geddes exercices and exemples (See Geddes1992, chapter 7)*/

gex_a2 = x^8 + x^6 -3*x^4 -3*x^3 + 8*x^2 +2*x - 5$
gex_b2 = 3*x^6 + 5*x^4 - 4*x^2 - 9*x + 21$


gex_a16 = 9*x^5 +2*x^4*y*z -189*x^3*y^3*z + 117*x^3*y*z^2 + 3*x^3 -42*x^2*y^4*z^2 +26*x^2*y^2*z^3 
	  +18*x^2 - 63*x*y^3*z + 39*x*y*z^2 + 4*x*y*z + 6$
gex_b16 = 6*x^6 - 126*x^4*y^3*z + 78*x^4*y*z^2 +x^4*y + x^4*z + 13*x^3 -21*x^2*y^4*z - 21*x^2*y^3*z^2
          + 13*x^2*y^2*z^2 + 13*x^2*y*z^3 -21*x*y^3*z + 13*x*y*z^2 +2*x*y +2*x*z + 2 $

gex_a22 = x^3 - 3*x^2 + 2*x$
gex_b22 = x^3 + 6*x^2 + 11*x + 6$


gexo_a1 = 2*x^6 +5*x^5 +7*x^4 + 3*x^3 + 6*x^2 - 2*x +1$
gexo_b1 = 3*x^5 +3*x^4 + 6*x^3 - x^2 + 3*x - 4$

gexo_a2 = x^4 + x^3 - w$
gexo_b2 = x^3 + 2*x^2 + 3*w*x - w + 1$


gexo_a14a = 1206*x^10 + 1413*x^9 - 1201*x^8 + 2506*x^7 + 4339*x^6 + 12*x^5 + 1405*x^4 + 415*x^3 + 1907*x^2 - 588*x - 1818$
gexo_b14a =  402*x^9 + 203*x^8 + 402*x^7 + 103*x^6 - 704*x^5 + 1706*x^4 - 1196*x^3 - 313*x^2 + 710*x - 383$
