/**
 * 
 * Somes primitives for modular algorithms
 * 
 * June 2016
 * 
 * Noe Brucy, ENS Rennes,
 * Intern at IMCCE, team ASD
 * Under supervision of Jacques Laskar and Mickael Gastineau
 */

_pind = 1 $ // Current prime in the vector

/**
 *@brief Returns a prime number 
 * 
 *Consecutive calls return different prime.
 *
 * @return a prime number
 */
macro new_prime[]
{
  _pind = _pind + 1 $
  
  if(_pind > sup(primes, 1)) then {_pind = inf(primes, 1) $ } $
    
  return int(primes[_pind])$
    
}$

/**
 * @brief Extended Euclidean Algorithm in Z
 * 
 * @param p q are integers
 * @param d is an indentifier used to store gcd.
 * @param s,t are indentifiers used to store Bezout coefficient, ie the integers
 * such as ps + qt = d
 */
macro EEA[a, b, d, s, t]
{  
  __coef_gcdext__(a, b, d, s, t) $
}$

/**
 * @brief  Euclidean Algorithm in K[X]
 * 
 * @param A, B are polynoms over a field K (K = Q or Fp) of the same variable
 * @param d is an indentifier used to store gcd.
 * @param s,t are indentifiers used to store Bezout coefficients ie the polynomes
 * such as ps + qt = d
 */
macro EEA_poly[A, B, d, s, t]
{
  private _ALL$
  __coef_gcdext__(A, B, d, s, t) $
  
  g = %lcoeff[d] $
}$



/**
 *@brief Returns c in Z such as c = a mod p and c = b mod q
 * 
 *@params p,q are coprimes integers
 *@params a, b are integers
 * 
 *@return c in Z such as c = a mod p and c = b mod q
 * 
 */
macro IntegerCRA[p, q, a, b]
{  
  private _ALL$
  
  %EEA[p, q, [d], [s], [t]] $ 
  
  if (d != 1) then { p;q; d;error("IntegerCRA, not coprimes")$} $
  
  a_mod = %symetric_mod[a, p] $
  c = %symetric_mod[s*(b - a_mod), q] $
  
  return %symetric_mod[a_mod + c*p, p*q] $
}$

/*
 *@brief Computes a polynomial C in K such as C = v1 mod P1 and C = v2 mod P2
 * 
 *@params P1, P2 are coprimes univariate polynomials over a field K = Q or Fp of the same variable x.
 *@params v1, v2 are polynomials in K[x1, ..., xn, x].
 * 
 *@return C in K[x1, ..., xn, x] such as C = v1 mod P1 and C = v2 mod P2
 */
macro PolyCRA[P1, P2, v1, v2]
{  
  private _ALL$

  %EEA_poly[P1, P2, [d], [s], [t]] $ 
  
  
  if (d != 1) then { error("PolyCRA, P1 and P2 should be coprimes")$} $
    
  //"v1"; v1; "P1"; P1;
  
  div(v1, P1, q, v1_mod) $

  div(s*(v2 - v1_mod), P2, q, c) $
  
  return v1_mod + c*P1 $
}$

/*
 *@brief Performs division of A by B in Zm[x] wher m and lcoeff[B] are coprimes
 * 
 *@param A, B are polynomials
 *@param m is an integer
 * 
 *@param Q, R are identifiers.
 * 
 *@return C in K[x1, ..., xn, x] such as C = v1 mod P1 and C = v2 mod P2
 */
macro div_Zm[A, B, Q, R, m] 
{
  private _ALL$
  
  a = %lcoeff[B] $
  %EEA[a, m, [d], [s], [t]] $
  if (d != 1) then { error("div_Zm, lcoeff(B) not unit")$} $

  Anew = %symetric_mod_poly[s*A, m] $
  Bnew = %symetric_mod_poly[s*B, m] $
  
  div(Anew, Bnew, Q, R) $
  
  Q = %symetric_mod_poly[Q, m] $
  R = %symetric_mod_poly[a*R, m] $
}$
  

