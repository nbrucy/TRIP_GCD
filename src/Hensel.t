
/** 
 * Hensel lifting, an algorithm to lift factorization in Zp[x]
 * to factorization in Zp^l[x1, ..., xk]
 * 
 * June 2016
 * 
 * Noe Brucy, ENS Rennes,
 * Intern at IMCCE, team ASD
 * Under supervision of Jacques Laskar and Mickael Gastineau
 * 
 * Ref :

    @inbook{Geddes1992_ch6,
    address = {Boston, MA},
	author = {Geddes, K. O. and Czapor, S. R. and Labahn, G.},
	chapter = {Newton's iteration and the Hensel construction},
	doi = {10.1007/978-0-585-33247-5_7},
	isbn = {978-0-585-33247-5},
	pages = {205--277},
	publisher = {Springer US},
	title = {Algorithms for Computer Algebra},
	url = {http://dx.doi.org/10.1007/978-0-585-33247-5_7},
	year = {1992}
    }
 *
 */


/** /!\ TODO Implement Mignotte's bound. */
macro coef_bound[P]
{
  private _ALL $ 
  vars = coef_tabvar(P) $
  n = %total_degree_lc[P, [vars]] $
  return 2**n  * __coef_norm_infinity__(P) $
}$

/**
 * @brief replace only the leading coefficient of a serie.
 * 
 * @param P is an univariate polynomial
 * @param new is the new leading coefficient
 */
macro replace_lc[P, new] 
{
  private _ALL $
   
  vars = coef_tabvar(P) $
  
  if(size(vars) != 0) then 
  {
    
    x := vars[1] $
    
    n = puismax(P, x) $
    old = coef_ext(P, (x, n)) $
    
    return P + (new - old)*x^n $
  } else
  {
    return new $
  }$
}$

/**
 * @brief Computes Hensel lifting for univariate polynomials.
 * Geddes1992, p 233
 * 
 * @param a_ is a primitive polynomial in Z[x] 
 * 
 * @param p is a prime integer which does not divide lcoeff(a(x))
 * 
 * @params u1_, w1_ are two coprimes polynomial in Zp[x] such as a = u1 * w1 mod p
 *
 * @param B is an integer bound for the magnitude of coefficients f a and any of 
 * its factor with degree less than max(deg(u1), deg(w1))
 *
 * @param gamma (optional, can be set to 0) is an integer known to be a multiple 
 * of lcoeff(u), where a = uw (see below).
 * 
 * @param u, w are indentifiers
 *  At the end,
 * 	u and w will verify a = uw in Z[x] and n(u) = n(u1) mod p, n(v) = n(v1) mod p,
 * 	where n is the monic normalization in Zp.
 * 	
 * 	Raise an error if no such factorization exists.
 * 
 */ 
macro UnivariateHensel[a_, p, u1_, w1_, B, gamma_, u, w] 
{
  private _ALL $
  
  /*  Step 1 : Define new polynomial and its modulus p factors 
   * (if a_, u1_, u2_ not monic)
   */
  
  alpha = %lcoeff[a_] $ // in Z[x]
  if (gamma_ == 0) then {gamma = alpha$} else {gamma = gamma_$} $
  
  a =  gamma * a_ $ 
  u1 = (gamma / %lcoeff[u1_]) * u1_ $ // in Zp[x]
  w1 = (alpha / %lcoeff[w1_]) * w1_ $ // in Zp[x]
  
  /* Step 2 :  EEA
   */
  
  %EEA_poly[u1, w1, [d], [s], [t]] $ // in Zp[x]
  
  if (d != 1) then {u1; w1; d; error("UnivariateHensel : u1 and w1 must be coprimes")$ }$
  
  /* Step 3 : Initialisation of the iteration
   */

  u = %replace_lc[__coef_Zp_to_Z__(u1), gamma] $ // in Z[x]
  w = %replace_lc[__coef_Zp_to_Z__(w1), alpha] $ // in Z[x]

  e = a - u*w $ // in Z[x]
  modulus = p $
  k = 1 $
  
  /* Step 4 Iterate until the correct factorization 
   * in Z[x] is obtained or the boiund 2*B is reached. 
   */
  while /*(*/(e != 0)/* && (modulus < 2*B*gamma))*/ do  // TODO Better bound --  inifite recursion
  {
    /* Step 4.1 : solve in Zp[X] the equation sigma*u1 + tau*w1 = x mod p, 
     * where sigma and tau are the inderterminates and c = e / modulus (computed 
     * in Z[x]).
     */
    c = e / modulus $ // in Z[x]
    c = Mod(c, p) $ // in Zp[x]
    sigma = s * c $// in Zp[x]
    tau = t * c $ // in Zp[x]
   
    /* Correcting sigma to fit degree requirement (deg(sigma) < deg(w1))*/
    div(sigma, w1, q, r) $ // in Zp[x]
    sigma = r $// in Zp[x]
    tau = tau + q * u1 $ // in Zp[x]
    
    /* Step 4.2 : Update the factors and compute the error
     */
    u = u + __coef_Zp_to_Z__(tau) * modulus $ // in Z[x]
    w = w + __coef_Zp_to_Z__(sigma) * modulus $ // in Z[x]
    e = a - u*w $ // in Z[x]
    modulus = modulus * p $ // in Z[x]
    k = k +1 $
  }$
  
  /* Step 5 : Check terminaison status 
   */ 
  if (e == 0) then {
    /* The factorization is correctly computed, and needs content removal */
    __coef_primitive_cont__(u, u, cont_u) $ // in Z[x] 
    w = w / (gamma/cont_u)  $ // in Z[x]
  } else {
    e;
    se = %symetric_mod_poly[e, modulus];
    a; u; w; 
    error("UnivariateHensel : No such factorization exists") $
  } $
} $

/**
 * @brief Computes s, t such that s * a + t * b = 1 mod (p^k)
 * whith deg(s) < deg(b) and deg(t) < deg(a)
 *
 * @param p is a prime integer.
 * @param A, B are two  univariate polynomials of Z[x].
 * @param k >= 1 is an integer. 
 * @param s, t are indentifiers
 * 
 * Assumption : p doesn't divide lcoeff[A] neither lcoeff[B]
 * A mod p and B mod p are coprimes in Fp[x].
 */
macro EEA_lift[A, B, p, k, s, t]
{ 
  private _ALL $
  /* Step 1 : Initialization
   */

  Amod = Mod(A, p) $ // in Zp[x]
  Bmod = Mod(B, p) $ // in Zp[x]
  
  %EEA_poly[Amod, Bmod, [d], [smod], [tmod]] $ // in Zp[x]
  
  if (d != 1) then { error("EEA_lift, not coprimes");} $
    
  s = __coef_Zp_to_Z__(smod) $ // in Z[x]
  t = __coef_Zp_to_Z__(tmod) $ // in Z[x]
  modulus = p $
  
  /* Step 2 : Newton iteration 
   */
  for j = 1 to k - 1
  { 
    e = %symetric_mod_poly[1 - s*A - t*B, modulus*p]  $ // in Z[x]
    /* The equation for iteration is (e/modulus) = (ds/modulus)*A + (dt/modulus)*B
     * in Zp[x], so we re-use the results computed during the Initialization step */
    c = e / modulus $// in Z[x]

    c = Mod(c, p) $// in Zp[x]
    sigma = smod * c $ // in Zp[x]
    tau = tmod *c $ // in Zp[x]
        
    /* Correcting sigma to fit degree requirement */
    div(sigma, Bmod, q, sigma) $ // in Zp[x]
    tau = tau + q * Amod $ // in Zp[x]
    
    /* Updating s and t */
    s = s + __coef_Zp_to_Z__(sigma) * modulus $ // in Z[x]
    t = t + __coef_Zp_to_Z__(tau) * modulus $ // in Z[x]
    
    modulus = modulus * p $
  }$
  
  if(%symetric_mod_poly[s*A + t*B, modulus] != 1) then {error("EEA_lift : result is false")$}$
}$


/**
 * @brief Find the unique solution in Zp^k[x] of the univariate diophantine equation 
 *                        sigma * A + tau * B = C mod p^k 
 * with deg(sigma) < deg(B)
 *
 * @param A, B, C are 3 univariate polynomials of Z[x].
 * @param p is a prime integer.
 * @param k is an integer
 * @param sigma, tau are indentifiers
 * 
 * Assumptions : 
 *  * p doesn't divide lcoeff[A] neither lcoeff[B]
 *  * A mod p and B mod p are coprimes in Fp[x].
 * 
 */
macro UnivariateDiophant[A, B, C, p, k, sigma, tau]
{
  private _ALL $
  
  modulus = p^k $
  
  vars = coef_tabvar(A) $
  x := vars[1] $
  
  %EEA_lift[A, B, p, k, [s], [t]] $
  
  %div_Zm[s*C, B, [q], [sigma], modulus] $
  sigma = %symetric_mod_poly[sigma, modulus] $
  
  tau = t*C + q*A $
  tau = %symetric_mod_poly[tau, modulus] $
  
  zero =  %symetric_mod_poly[sigma*A + tau*B - C, modulus] $
  
  if(zero != 0) then {error("UnivariateDiophant : result is false")$}$

}$

    
/**
 * @brief coeff of (x - alpha)^m in the Taylor expansion of P about x = alpha 
 */
macro taylor[P, x, alpha, m,  modulus] 
{
  private _ALL $
  
// div(P, (x - alpha)^m, Pm , r) $
// return  %symetric_mod_poly[coef_num(Pm, (x, alpha)), modulus] $
  P2 = %symetric_mod_poly[subst(P, x, x + alpha), modulus] $
  return %symetric_mod_poly[coef_ext(P2, (x, m)), modulus] $
}$


/**
 * Find the unique solution of the multivariate polynomial diophantine equation
 * 
 *    sigma * A + tau * B = C mod (J^{d + 1}, p^k)
 * and deg_x1_(sigma) < deg_x1_(B)
 * 
 * in the domain Zp^k[x1, ..., xv].
 * 
 * 
 * @param A, B, C are polynomials in Z[x1, ..., xn]
 * @param J is a tab of integer values [0, a2, a3, ..., an]. 
 * It represents the ideal (x2 - a2, x3 - a3, ..., xn - an)
 * @param d is a positive integer, specifying the maximum degree 
 * (wrt x2, .. xn) of the desired result.
 * @param p is a prime integer
 * @k is a postive integer (arithmetic has to be done modulus p^k)
 * @param sigma, tau are indentifiers
 * 
 * Assumptions : 
 * * p doesn't divide lcoeff[A] neither lcoeff[B]
 * * A mod p and B mod p are coprimes in Fp[x].
 * * deg_x1_(C) <= deg_x1_(A) + deg_x1_(B)
 */


macro MultivariateDiophant[A, B, C, vars,  J, d, p, k, sigma, tau]
{

  private _ALL $
  
  
  
  /* Step 1 : Initialization
   */
  
  n = size(vars) $
  x := vars[n] $
  alpha = J[n] $
  modulus = p^k $
  
  if (n > 1) then
  {
    
    /* Step 2.1 : Multivariate case
     */
    dimvar rem_vars[1:n-1] $
    rem_vars := vars[1:n-1] $ /* vars for recursive calls */
        
    Anew = coef_num(A, (x, alpha)) $
    Bnew = coef_num(B, (x, alpha)) $
    Cnew = coef_num(C, (x, alpha)) $
    
    dim Jnew [1:n-1] $
    Jnew = J[1:n-1] $
    %MultivariateDiophant[Anew, Bnew, Cnew, [rem_vars], Jnew, d, p, k, [sigma], [tau]] $
    sigma = %symetric_mod_poly[sigma, modulus] $
    tau = %symetric_mod_poly[tau, modulus]  $
    e = %symetric_mod_poly[C - A*sigma - B*tau, modulus] $
    
    monomial = 1 $
    m = 1 $
    
    while ((e != 0) && (m <= d)) do
    {
      monomial = monomial * (x - alpha) $
      
      Cm = %taylor[e, [x], alpha, m, modulus] $
      
      if (Cm != 0) then 
      {
        %MultivariateDiophant[Anew, Bnew, Cm, [rem_vars], Jnew, d, p, k, [dsigma], [dtau]] $
        
        dsigma = dsigma *  monomial $
        dtau = dtau *  monomial $

        sigma = %symetric_mod_poly[sigma + dsigma, modulus] $
        tau = %symetric_mod_poly[tau + dtau,modulus] $
        e = %symetric_mod_poly[C - A*sigma - B*tau, modulus] $
      
      } $
      
      m = m + 1 $
    }$
  }
  else {
    /* Step 2.2 : Univariate case
     * 
     */     
    %UnivariateDiophant[A, B, C, p, k, [sigma], [tau]] $
  }$
  
  if(%symetric_mod_poly[sigma*A + tau*B - C, modulus] != 0) then {error("MultivariateDiophant : result is false")$}$
}$


/**
 * @brief Hensel lifting, an algorithm to lift factorization in Zp[x]
 * to factorization in Zp^l[x1, ..., xk]
 * 
 * @param A is a polynomial of Z[x1, ..., xn], primitive if seen as a polynomial
 * of Z[x2, ...xk][x1]
 * 
 * @param J is a tab of integer values [0, a2, a3, ..., an]. 
 * It represents the ideal (x2 - a2, x3 - a3, ..., xn - an)
 * 
 * @param p is a prime integer which does not divide lcoeff_x1_(A)
 * 
 * @param l is a prime integer such as p^l bounds the magnitude of all integers
 * appearing in A and in any of its factors to be computed
 * 
 * @param  U1, W1 are two polynomials in Zpl[x1], coprimes in Zp[x1], such as
 *  a = u1w1 mod (J, p^l)
 * 
 * @param lcU, lcW : correct multivariate leading coefficient of the factors U and W
 * (in Z[x2, ...xk])
 * 
 * @param U, W are indentifiers
 * 	U and W verify A = UW in Z[x1, ..., xn] and 
 *  U/lcoeff_x1_(U) = U1/lcoeff_x1_(U1) mod(J, p^l)
 *  W/lcoeff_x1_(W) = W1/lcoeff_x1_(W1) mod(J, p^l)
 * 
 * 	Raise an error if no such factorization exists.
 * 
 */
macro MultivariateHensel[A, vars, J, p, l, U1_, W1_, lcU, lcW, U, W] 
{
  private _ALL $
  
  U = 0 $
  V = 0 $
  
  /** Step 1 : Initialization for the multivariate iteration
   */
  
  n = size(vars) $
  modulus = p^l $
  
  /* Storing succesive evaluations */
  dim Aeval[1:n] $
  
  Aeval[n] = A $ 
  
  for j = n to 2 step -1 
  {
    Aeval[j - 1] = coef_num(Aeval[j], (vars[j], J[j])) $
    // Aeval[j] is A mod ((xn - an), ..., (x_{j+1} - a_{j + 1}))
  }$

  maxdeg = %max_degree[A, vars[2:n]] $
  U = U1_ $
  W = W1_ $
  
  
  /** Step 2 : Variable by variable Hensel iteration
   */
  for j = 2 to n 
  { 
    x := vars[j] $
    dimvar rem_vars[1:j-1] $
    rem_vars := vars[1:j-1] $
    
    alpha = J[j] $
    
    U1 = U $ 
    W1 = W $
    monomial = 1 $
    
    e =  %symetric_mod_poly[Aeval[j] - U*W, modulus] $
    Cpre = %taylor[e, [x], alpha, 0, modulus] $
    if (Cpre != 0) then 
    {
      Cpre;
      error("MultivariateHensel : First pretest failed");
    }$
  
    /* Replace leading coefficient */
    if (lcU != 1) then 
    {
      if (j != n) then 
      {
        coef = coef_num(lcU, (vars[j + 1 : n], J[j + 1 : n])) $
      } else
      { 
        coef = lcU $
      }$
      
      coef = %symetric_mod_poly[coef, modulus]$
      U = %replace_lc[U, coef] $
    }$
    
    if (lcW != 1) then 
    {

      if (j != n) then 
      {
        coef = coef_num(lcW, (vars[j + 1 : n], J[j + 1 : n])) $
      } else
      { 
        coef = lcW $
      }$
      
      coef = %symetric_mod_poly[coef, modulus] $
      W = %replace_lc[W, coef] $
    }$
    
    
    /* Iterate until the ideal (x - alpha)^k is large enough
      to contain the researched U and W */

    e =  %symetric_mod_poly[Aeval[j] - U*W, modulus] $
    k = 1 $
    
    
    while((e != 0) && (k <= puismax(Aeval[j], vars[j]))) do 
    {
      monomial = monomial * (x - alpha) $
      
      
      Cpre = %taylor[e, [x], alpha, k - 1, modulus] $
      if (Cpre != 0) then 
      {
        k - 1;
        error("MultivariateHensel : Step pretest failed");
      }$
            
      C = %taylor[e, [x], alpha, k, modulus] $
      
      if (C != 0) then 
      {
        %MultivariateDiophant[U1, W1, C, [rem_vars], J[1: j-1], maxdeg, p, l, [dW], [dU]] $
        
        dU = dU * monomial $
        dW = dW * monomial $
        
        U = %symetric_mod_poly[U + dU, modulus] $
        W = %symetric_mod_poly[W + dW, modulus] $
        

        e = %symetric_mod_poly[Aeval[j] - U*W, modulus] $ 
        
      }$
      
      Cpost = %taylor[e, [x], alpha, k, modulus] $
      if (Cpost != 0) then 
      {
        error("MultivariateHensel : Step posttest failed");
      }$
      
      k = k + 1$
    }$
  }$
  
  /** Step 3 : Check terminaison status
   */
    
  if (A - U*W == 0) then
  {
    return U $
  } else
  {
    A; U; W; 
    error("MultivariateHensel : No such factorization exists") $
    /* If this happens in EZ-GCD, the cause is probably a bad homomorphism. */
  } $  
} $


/**  
 * 
 * Warning : the results are given modulo a favtor in Z[y1, ..., y_n],
 * hence primitivization has to be performed then. 
 */
macro EZ_LIFT[A, UI, WI, p, b, vars, lcA, mlcU, U, W]
{
  private _ALL $
  
  n = size(vars) $
  x := vars[1] $
  dimvar coef_vars[1:n-1] $
  coef_vars := vars[2:n] $
  J = [0:b] $
  
  Ab = coef_num(A, (coef_vars, b)) $
  lcAb = %lcoeff[Ab] $
  mlcUb = coef_num(mlcU, (coef_vars, b)) $
  
  bound = %coef_bound[Ab] $
  %UnivariateHensel[Ab, p, UI, WI, bound, mlcUb, [Ub], [Wb]] $
  
  mA =  mlcU * A $ 
  mUb = (mlcUb / %lcoeff[Ub]) * Ub $// in Zp^l[x]
  mWb = (lcAb / %lcoeff[Wb]) * Wb $ // in Zp^l[x]

  
  bound = %coef_bound[mA] $
  l = int(log(bound) / log(p)) + 1 $

  %MultivariateHensel[mA, [vars], J, p, l, mUb, mWb, mlcU, lcA, [U], [W]] $
}$






 

