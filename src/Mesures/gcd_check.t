include("includes.t")$
include("gcdmultivarbench_gen.t")$
include("test_cases_auto.t")$

macro manual_check[test_set, ...] 
{ 
  private _ALL $
  ! "echo ""Manual check"" > check_results"$
  
  nbmeth = size(macro_optargs) $ 
  dimvar meths[1:nbmeth]$

  for j = 1 to nbmeth 
  {
    meths[j] := macro_optargs[j] $
  }$

  for k = inf(test_set, 1) to sup(test_set, 1)
  {
    msg("Test %d \n", k) $
    for j = 1 to nbmeth {
      meth := meths[j] $
      try
      {
        %meth[test_set[k,:]] $
        ! "echo """ + msg("Test %d, Method %d : OK", k, j) + """ >> check_results";
      } catch
      {
        ! "echo """ + msg("Test %d, Method %d : FAILURE",  k, j) + """ >> check_results";
      }$
    }$
  }$
  
}$


macro random_check[...]
{
  private _ALL $
  public NB_SAMPLE $
  
  VARMAX = 6 $
  DEGGCDMAX = 4 $
  DEGCOFMAX = 4 $
  SCOEFFMAX = 5 $ 
  NBTERMSMAX = 4 $
  
  maple;
    seed := randomize():
  end;

  seed =0$
  maple_get(seed) $

  ! "echo """ + msg("seed = %d", seed) + """ > check_results"$
  
  nbmeth = size(macro_optargs) $ 
  dimvar meths[1:nbmeth]$

  for j = 1 to nbmeth 
  {
    meths[j] := macro_optargs[j] $
  }$

  
  for nbvars = 2 to VARMAX 
  {
    for degregcd = 0 to DEGGCDMAX  
    {      
      for degrecof = 0 to  DEGCOFMAX 
      {
        for scoeff = 0 to  SCOEFFMAX 
        {
          for nbterms = 0 to NBTERMSMAX 
          {
            for k = 1 to NB_SAMPLE 
            {
              dim h[1:3]$
              h = %sample[nbvars, 2**degregcd - 1, 2**degrecof - 1, 2**nbterms, 2**scoeff] $
              for j = 1 to nbmeth 
              {
                meth := meths[j] $
                try
                {
                  %meth[h] $
                   ! "echo """ + msg("%d vars, degree GCD =  %d, degree COF = %d, size = %d, terms = %d, meth = %d : OK", nbvars, 2**degregcd - 1, 2**degrecof - 1, 2**scoeff, 2**nbterms, j) + """ >> check_results";
                } catch
                {
                 afftab(h) ;
                 ! "echo """ + msg("%d vars, degree GCD =  %d, degree COF = %d, size = %d, terms = %d, meth = %d : FAILURE", nbvars, 2**degregcd - 1, 2**degrecof - 1, 2**scoeff, 2**nbterms, j) + """ >> check_results";
                }$
              }$
            }$
          }$
        }$
      }$
    }$
  }$
}$

