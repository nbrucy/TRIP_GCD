

macro PRS_GCD[A, B, div_beta, self_GCD]
{
  private _ALL$
  


  %get_union_vars[A, B, [vars]] $
  
  if (size(vars) <= 1) then 
  {
    return %UGCD[A, B] $ 
    stop $
  } $
  
  x := vars[1] $

  delta = puismax(A, x) - puismax(B, x)$
  
  if(delta < 0) then 
  {
    return  %self_GCD[B, A] $
    stop $
  } $
  
  %primitive_cont_over_poly[A, [r0], [cont_A], [x], [self_GCD]] $
  %primitive_cont_over_poly[B, [r1], [cont_B], [x], [self_GCD]] $

  cont_G = %self_GCD[cont_A, cont_B] $
  
  psi = -1 $
  
  alpha1 = %lc[r1, [x]]^(delta + 1) $ 
  alpha0 = alpha1 $
  
  niter = 1 $
  
  while(r1 != 0) do 
  {
    div(alpha1 * r0, r1, q, r2) $
          
    aux = r1 $
    
    psi = ((-%lc[r0, x])^delta)*(psi^(1 - delta)) $
    r1 = %div_beta[niter, r0, r2, delta, alpha0, psi, [x]] $

    r0 = aux $
    delta = puismax(r0, x) - puismax(r1, x) $

    alpha0 = alpha1 $
    alpha1 = %lc[r1, x]^(delta + 1) $
    
    niter = niter + 1 $
    
  }$      
  
  %primitive_cont_over_poly[r0, [G], [trash], [x], [self_GCD]] $
  return cont_G * G $
}$

macro beta_euclidian[niter, r0, r2, delta, alpha, phi, x] 
{
  private _ALL$
  return r2 $
} $

macro beta_primitive[niter, r0, r2, delta, alpha, phi, x] 
{
  private _ALL$
  if (r2 == 0) then 
  { return r2 $} 
  else 
  {
    %primitive_cont_over_poly[r2,  [pp], [cont], [x], [primitive_PRS_GCD]] $
    return pp $
  }$
} $

macro beta_reduced[niter, r0, r2, delta, alpha, phi, x] 
{
  private _ALL$
  if (niter == 1) then 
    {return r2 $} 
  else 
    {return r2/alpha $} $
} $

macro beta_subresultant[niter, r0, r2, delta, alpha, psi, x] 
{
  private _ALL$
  if (niter == 1) then 
    {return r2 * ((-1)^(delta + 1)) $} 
  else 
    {(-%lc[r0, x]*psi^delta)$ return r2 /(-%lc[r0, x]*psi^delta) $} $
} $

macro euclidian_PRS_GCD[A, B]
{ 
  private _ALL$
  return %PRS_GCD[A, B, [beta_euclidian], [euclidian_PRS_GCD] ] $
}$

macro primitive_PRS_GCD[A, B]
{ 
  private _ALL$
  return %PRS_GCD[A, B, [beta_primitive], [primitive_PRS_GCD] ] $
}$


macro reduced_PRS_GCD[A, B]
{ 
  private _ALL$
  return %PRS_GCD[A, B, [beta_reduced], [reduced_PRS_GCD]] $
}$


macro subresultant_PRS_GCD[A, B]
{ 
  private _ALL$
  return %PRS_GCD[A, B, [beta_subresultant], [subresultant_PRS_GCD]] $
}$



