PATH = "../"$

include PATH + "MGCD.t" $ 
include  PATH +  "GCDHEU.t" $

vnumR primes $ // vector of primes number for the macro new_prime
read( PATH + "minip.dat", primes) $

include  PATH +  "fun_poly.t" $ 
include  PATH +  "modular.t" $ 
include  PATH +  "test_cases.t" $

include  PATH + "SparseMod.t" $
include  PATH + "Hensel.t" $
include  PATH + "EEZ-GCD.t" $
include  PATH + "PRS.t" $
