


macro plot_grid[title_grid] 
{
  private _ALL$
  title_data = title_grid + ".dat" $
  
  plotps title_grid + ".pdf";
  gnuplot;
  set term  pdf color
  end;
  
  gnuplot;
  set cbrange[0:400];
  set palette defined (0 'red', 1 'orange', 1.1 'yellow', 10  'green', 400 'blue')  
  set xlabel 'degré du pgcd'
  set ylabel 'degré des cofacteurs'
  set xtics 0,5,30
  set ytics 0,5,30
  set title 'temps(Maple avec transfert)/temps(GCDHEU)'
  plot '@title_data@'  matrix  with image notitle ;
  end;
  plotps_end;
};
