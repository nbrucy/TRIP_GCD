/** 
 * SparseMod, a probabilistic modular algorithm to compute GCD of multivariate polynomials over integers.
 * Zippel 1979
 * 
 * June 2016
 * 
 * Noe Brucy, ENS Rennes,
 * Intern at IMCCE, team ASD
 * Under supervision of Jacques Laskar and Mickael Gastineau
 * 
 * Ref :
 * 
    @inproceedings{Zippel1981,
        acmid = {806372},
        address = {New York, NY, USA},
        author = {Zippel, Richard},
        booktitle = {Proceedings of the Fourth ACM Symposium on Symbolic and Algebraic Computation},
        doi = {10.1145/800206.806372},
        isbn = {0-89791-047-8},
        location = {Snowbird, Utah, USA},
        numpages = {5},
        pages = {68--72},
        publisher = {ACM},
        series = {SYMSAC '81},
        title = {Newton's Iteration and the Sparse Hensel Algorithm (Extended Abstract)},
        url = {http://doi.acm.org/10.1145/800206.806372},
        year = {1981}
    }
 *
 *
 */

macro rand_n_tuple[p, n]
{
  dim n_tuple[1:n] $
  
  for j = 1 to n 
  {
    n_tuple[j] = random(p) $
  }$
  
  return n_tuple $
} $

/**
 * @brief Solves a sytem of linear equation using Gauss Jordan algorithm
 * 
 * @param S_ is a tab of series. The system is given by : 
 * 	for all 1 <= i <= size(S_), S_[i] = 0.
 * 
 * @param vars are the inderminates.
 */
macro solveGJ[S_, vars]
{
  private _ALL $
  S = S_ $
  dim v[inf(S, 1) : sup(S, 1)]$ // Virtual mapping of indexes
  dim T[inf(vars, 1):sup(vars, 1)]$
  
  if (size(S_) < size(vars)) then {error("solveGJ : Not enough equation")$}$
  
  for j = inf(S, 1) to sup(S, 1) 
  {
    v[j] = inf(vars, 1) - inf(S, 1)  + j $
  }$
  
  for j = inf(vars, 1) to sup(vars, 1) 
  {
    k = j $
    
    a = coef_ext(S[v[k]], (vars[j], 1))$
    
    while (a == 0) do
    {
      k = k + 1$
      if(k > sup(S, 1)) then {error("Singular Matrix")$ }$
      a = coef_ext(S[v[k]], (vars[j], 1))$
    }$
    aux = v[j] $
    v[j] = v[k] $
    v[k] = aux $
    S[v[j]] = S[v[j]] / a $
    for k = j + 1 to sup(S, 1)
    {
      f = coef_ext(S[v[k]], (vars[j], 1)) $
      S[v[k]] = S[v[k]]  - f * S[v[j]] $
    }$
  }$
  
  for j = sup(vars, 1) to inf(vars, 1) + 1 step -1
  {
    for k = j - 1 to inf(vars, 1) step -1
    {
      f = coef_ext(S[v[k]], (vars[j], 1)) $
      S[v[k]] = S[v[k]]  - f * S[v[j]] $
    }$
  }$

  for j = inf(vars, 1) to sup(vars, 1) 
  {
    T[j] = - coef_ext(S[v[j]], (vars[j], 0)) $
  }$

  return T $
}$
  

macro SparseInter[A, B, skeleton, p]
{
  private _ALL $
  
  if(size(skeleton) <= 1) then 
  { 
    /* GCD are defined modulo an unit so this case is really easy */
    return skeleton[inf(skeleton, 1)] $
    stop $
  } $

  %get_union_vars[A, B, [vars]] $
  /*  Create inderminates s */
  dimvar s[inf(skeleton, 1) + 1: sup(skeleton, 1)] $
  tabvar(s) $
  
  /* Create a polynom P with the appropriate form */
  dim tcs[inf(skeleton, 1): sup(skeleton, 1)] $
  for j = inf(skeleton, 1) + 1 to sup(skeleton, 1) {
    tcs[j] = Mod(s[j], p) $
  } $
  tcs[inf(skeleton, 1)] = Mod(1, p) $
  P = invcoef_tab(tcs, skeleton) $
  
  /* Detecting and solving the "monomial in leading var" problem */
  coef_tab(P, tc, tm, vars[1]) $
  
  while(size(tc) == 1) do {
    /* Changing leading variable */
    x:= vars[1] $
    for j = 1 to size(vars) - 1
    {
      vars[j] := vars[j + 1] $
    }$
    vars[size(vars)] := x$
    coef_tab(P, tc, tm, vars[1]) $
  }$
  
  /* Initialize the tab of linear equations */
  dim S[1:1] $
  S[1] = Mod(0, p)$
  
  /* Set up the system to have enough independant linear equations*/
  for j = 1 to size(s) {
    
    /* Choose random evaluation point for all but one variable */
    eval_point = %rand_n_tuple[p, size(vars) - 1] $

    /* Compute the image of the GCD modulo an unit */
    A_= coef_num(A, (vars[2:], eval_point)) $
    B_ = coef_num(B, (vars[2:], eval_point)) $
    C_ = %UGCD[A_, B_] $ // Normalized
    
    /* Compute the image of the generic polynom */
    P_ = coef_num(P, (vars[2:], eval_point)) $
    
    /* Putting the right leading coefficient for the comparaison */
    C_ = C_ * coef_ext(P_, (vars[1], puismax(P_, vars[1]))) $ 

    /* P_ and C_ has to be equal : this gives us a set of linear equations */
    coef_tab(P_ - C_, tc, tm, vars[1]) $
    
    /* Add the new equations to the system */
    S = [S : tc] $
  } $
  
  /* Remove the first equation (useless) */ 
  S = S[2:] $
  
  // TODO ? Test for independance ??
  
  /* Solve the system */
  T = %solveGJ[S, [s]] $
    // or roots_num(S, REAL, 0, T, R, E, s); ?
    
    
  /* Compute integer from the modular solutions */
  // coefnum for Mod would allow to delete that 
  for j = inf(T, 1) to sup(T, 1) 
  {
    T[j] = __coef_Zp_to_Z__(T[j]) $
  }$
  
  /* Use the solution to build the interpolated polynom */
  Cb = coef_num(P, (s, T)) $

  return Cb $
}$



macro SparsePGCD[A, B, p]
{
  private _ALL$
  %get_union_vars[A, B, [vars]] $
  
  if (size(vars) <= 1) then
  {
    return  %UGCD[A, B] $ // univariate case
    stop$
  } $
  
  x := vars[sup(vars, 1)] $
  dimvar rem_vars[1:sup(vars, 1) - 1] $
  rem_vars := vars[1:sup(vars, 1) - 1] $
  
  %primitive_cont_over_poly[A, [Ap], [cont_A], [rem_vars]] $
  %primitive_cont_over_poly[B, [Bp], [cont_B], [rem_vars]] $
  
  cont_G = %UGCD[cont_A, cont_B] $ // content of G
  g = %UGCD[%lcoeff_over_poly[Ap, [rem_vars] ], %lcoeff_over_poly[Bp, [rem_vars]]] $
  // cont_G and g are in Fp[x]
  
  one = Mod(1, p) $/* Computing 1 in the right field */
  
  // Main loop
  q = one $ H = one $
  e = min(%total_degree_lc[Ap, [vars]], %total_degree_lc[Bp, [vars]]) + 1$
  limit =  min(puismax(Ap, x), puismax(Bp, x))  + puismax(g, x)$ // Bound for the degree in x of G
  n_iter = 0$ // number of non unlucky evaluation points so far 
  
  b = 35 $ // the evaluation point is chosen arbitrarily
    
  while (0 == 0) do 
  {
    b = b + 1 $
    gb = coef_num(g, (x, b))$
    
    while(gb == 0) do 
    {
      b = b + 1 $
      gb = coef_num(g, (x, b))$
    }$
    
    Ab = coef_num(Ap, (x, b)) $
    Bb = coef_num(Bp, (x, b)) $
      
    if((n_iter == 0) || (size(vars) <= 2)) then 
    {
      Cb = %SparsePGCD[Ab, Bb, p] $ // Recursive call with n variables 
    } else
    {
      Cb = %SparseInter[Ab, Bb, skeleton, p] $
    }$
    
    d = %total_degree_lc[Cb, [vars]] $
    
    // Normalize Cb so that lcoeff(Cb) = gb
    Cb = gb * (1  / %lcoeff[Cb]) * Cb $
    
    mb = (Mod(x, p) - b) $
    
    // Test for unlucky homomorphism
    if ((n_iter == 0) || (d < e)) then 
    {
      q = mb $ 
      H = Cb $
      e = d $
      n_iter = 1 $ 
      coef_tab(H, tH, skeleton, rem_vars)$ 
    } else 
    { 
      if(d == e) then
      {
        new_H = %PolyCRA[q, mb, H, Cb] $
        n_iter = n_iter + 1 $
        q = q * mb $
        n_iter$ limit$
        // Test for completion
        if ((H == new_H) || (n_iter > limit)) then 
        {
          if (%lcoeff_over_poly[H, [rem_vars]] == g) then 
          {
            %primitive_cont_over_poly[new_H, [G], [none], [rem_vars]] $ 
            div(Ap, G, Q, R)$
            if (R == 0) then 
            {
              div(Bp, G, Q, R)$
              if (R == 0) then 
              {
            stop$
              }$
              
            }$
            // The skeleton was false, everything needs to be done again
            n_iter = 0 $
            "P test failed";
          }$
        }$
        H = new_H $
      }$
    }$ 
  }$
  return (cont_G * G) $
}$


macro SparseMGCD[A, B] 
{
  private _ALL$
  
  if (A == 0) then 
  {
    return B $
    stop $ 
  }$
  
  if (B == 0) then 
  {
    return A $
    stop $ 
  }$
  
  __coef_primitive_cont__(A, prim_A, cont_A) $
  __coef_primitive_cont__(B, prim_B, cont_B) $
  
  %get_union_vars[prim_A, prim_B, [vars]] $

  cont_G = __coef_gcd__(cont_A, cont_B) $
  
  g = __coef_gcd__(%lcoeff[prim_A], %lcoeff[prim_B]) $
  
  q = 0 $
  H = 0 $

  n = min(%total_degree_lc[prim_A, [vars]], %total_degree_lc[prim_B, [vars]])$ 
  e = n$
  
  n_iter = 0$
  
  min(__coef_norm_infinity__(prim_A), __coef_norm_infinity__(prim_B)) $

  limit = 2**n * abs(g) * min(__coef_norm_infinity__(prim_A), __coef_norm_infinity__(prim_B)) $
  
  while (0 == 0) do 
  {
    gp = 0 $

    while(gp == 0) do 
    {
      p = %new_prime $
      gp = Mod(g, p) $
    }$
    
    Ap = Mod(prim_A, p) $
    Bp = Mod(prim_B, p) $
      
    if((n_iter == 0) || (size(vars) <= 2)) then 
    {
      Cp = %SparsePGCD[Ap, Bp, p] $
    } else
    {
      Cp = %SparseInter[Ap, Bp, skeleton, p] $
    }$

    d = %total_degree_lc[Cp, [vars]] $

    Cp = (gp / %lcoeff[Cp]) *  Cp $ 

    if (d == 0) then 
    {
      G = 1 $
      stop$ 
    } $
    
    if ((n_iter == 0) || (d < e)) then 
    {
      q = p $
      new_H = __coef_Zp_to_Z__(Cp) $
      e = d $
      n_iter = 1$
      coef_tab(Cp, tH, skeleton, vars)$ 
    } else
    {
      if (d == e) then 
      {
	Cp = __coef_Zp_to_Z__(Cp) $
	
	hvars = coef_tabvar(H) $
	coef_tabexp(H, tcH, texpH, hvars)$
	coef_tab(H, tcH, tmH, hvars)$
	
	texpH = vnumtodim(texpH) $ 
	
	for j = inf(tcH, 1) to sup(tcH, 1) 
	{
	  tcH[j] = %IntegerCRA[q, p, tcH[j], coef_ext(Cp, (hvars, texpH[sup(texpH, 1) - j + 1,:]))] $
	}$
	
	new_H = invcoef_tab(tcH, tmH)$
	n_iter = n_iter + 1 $
	q = q * p $
      }$
    }$
    
    // Test for completion
    if ((H == new_H) || (q > limit)) then 
    {
      __coef_primitive_cont__(new_H, G, cont_H) $
      
      // test if G is the GCD of A and B by division
      div(prim_A, G, q1, r1) $
    
      
      if (r1 == 0) then 
      {
	div(prim_B, G, q2, r2) $
	
	if (r2 == 0) then 
	{
	  stop$
	} $
      } $
      
      n_iter = 0 $
      "SPmod test failed";
    } $
    
    H = new_H $
    

  } $
  return cont_G * G $
} $


