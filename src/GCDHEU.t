/** 
 * GCDHEU, a heuristic algorithm to compute GCD of multivariate polynomials over integers.
 * 
 * 
 * May 2016
 * 
 * Noé Brucy, ENS Rennes,
 * Intern at IMCCE, team ASD
 * Under supervision of Jacques Laskar and Mickael Gastineau
 */


/** 
 * @brief Generates a polynom from xsi-adic expansion of P
 * @param P is a polynom wich not depends on x. 
 * @param x is a variable
 * 
 * 
 * @return a polynom construted from xsi-adic expansion of the coefficient of P
 * (eg : gen_poly[7y + 3, 3, x] -> 2*xy + x + y) 
 */
macro gen_poly[P, xsi, x]
{
  private _ALL $
  r = P $
  G = 0 $
  g = 0 $
  k = 0 $
  while (r != 0) do
    {
      g = %symetric_mod_poly[r, xsi] $
      G = G + g * x^k $
      r = (r - g) / xsi $
      k = k +1 $ 
    } $
  return G $
}$
  
/** This algorithm is implemented as in Geddes1992, p330 */
macro GCD[A, B]
{
  private _ALL $
  
  if (A == 0) then 
  {
    return B $
    stop $ 
  }$
  
  if (B == 0) then 
  {
    return A $
    stop $ 
  }$

  
  %GCDHEU[A, B, [G], [p_flag]] $
  
  if (p_flag != "Success") then 
  {
    error("GCDHEU Failed") $
  } $
  
  return G $
}$

macro GCDHEU[A, B, G, p_flag]
{
  private _ALL $
  %get_union_vars[A, B, [p_vars]] $
  
  if (size(p_vars) <= 1) then
  {
    G = __coef_gcd__(A, B) $
    p_flag = "Success" $
    stop $
  }$

  __coef_primitive_cont__(A, Ap, cont_A) $
  __coef_primitive_cont__(B, Bp, cont_B) $

  cont_G = __coef_gcd__(cont_A, cont_B) $
  %GCDHEU_aux[Ap, Bp, p_vars[1], [prim_G], [p_flag]] $

  G = cont_G * prim_G $
}$

macro GCDHEU_aux[A, B, x, G, p_flag]
{
  private _ALL $
  xsi = 2 * min(__coef_norm_infinity__(A), __coef_norm_infinity__(B)) + 2 $
  for j = 1 to 6 
  {
    if ((log(xsi) / log(2)) * max(puismax(A, x), puismax(B, x)) >= 50000) then
    {
//       p_flag = "Fail_HIGH_COEF" $
//       error("GCDHEU failed : coef too high")$
    }$
    
    // Evaluating and solving recursively
    %GCDHEU[coef_num(A, (x, xsi)), coef_num(B, (x, xsi)), [gamma], [flag_rec]] $
    
    
    if (flag_rec == "Success") then 
    {
      // Generate polynomial G from xsi-adic expansion of gamma
      G = %gen_poly[gamma, xsi, x]$
      
      __coef_primitive_cont__(G, G, cont_G) $

      // Then we test if G is the GCD of A and B by division
      div(A, G, q1, r1) $
      
      if (r1 == 0) then 
      {
        div(B, G, q2, r2) $
        
        if (r2 == 0) then 
        {
          p_flag = "Success" $	 
          stop;
        } $
      } $
    } $
        
    if (j== 6) then 
    {
      p_flag="Fail_MAX_TRY"$ 
      G = 1$
      stop;
    } $
    
  // Create new evaluation point using square of golden ratio  
  xsi = int((xsi* 73794) / 27011) $
  } $
} $
