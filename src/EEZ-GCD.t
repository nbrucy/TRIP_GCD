/** 
 * EEZ-GCD, an algorithm using Hensel's lifting
 * 
 * June 2016
 * 
 * Noe Brucy, ENS Rennes,
 * Intern at IMCCE, team ASD
 * Under supervision of Jacques Laskar and Mickael Gastineau
 * 
 * Ref :

    @inbook{Geddes1992_ch7,
    address = {Boston, MA},
	author = {Geddes, K. O. and Czapor, S. R. and Labahn, G.},
	chapter = {Polynomial GCD Computation},
	doi = {10.1007/978-0-585-33247-5_7},
	isbn = {978-0-585-33247-5},
	pages = {314--320},
	publisher = {Springer US},
	title = {Algorithms for Computer Algebra},
	url = {http://dx.doi.org/10.1007/978-0-585-33247-5_7},
	year = {1992}
    }
 *
 */


macro choose_new_prime[lcA, lcB, lcAmod, lcBmod] 
{
  private _ALL $
  
  p = %new_prime $
  lcAmod = Mod(lcA, p) $
  lcBmod = Mod(lcB, p) $
    
  while ((lcAmod == 0) || (lcBmod == 0)) do 
  {
    p = %new_prime $
    lcAmod = Mod(lcA, p) $
    lcBmod = Mod(lcB, p) $
  }$
  
  return p $
}$
  
macro choose_eval_point[lcA, lcB, p, vars, force_new]
{
  private _ALL $
  
  n = size(vars) $
  dim b[1:n] $
  
  lcAI = lcA $
  lcBI = lcB $
  
  for j = 1 to n 
  {
    /** We try with zero (best choice), and if it does not work 
     * we try a other thing */
    
    if(force_new != 0) then
    {
      put_zero = random(2) $ /* Probabilistic change */
      if(n == 1) then { put_zero = 1$} $ /* Fore renewal when n is small */
      b[j] = put_zero  * random(max(100, int(p / 2**10))) $
    }
    else
    {
      b[j] = 0 $
    }$
    
   
    lcAII = coef_num(lcAI, (vars[j], b[j])) $
    lcBII = coef_num(lcBI, (vars[j], b[j])) $
    
    while ((lcAII == 0) || (lcBII == 0)) do 
    {
      b[j] = random(max(100, int(p / 2**16))) $
      lcAII = coef_num(lcAI, (vars[j], b[j])) $
      lcBII = coef_num(lcBI, (vars[j], b[j])) $
    }$
    
    lcAI = lcAII$  
    lcBI = lcBII$
     
  }$

  return b $
}$

macro SGCD[A, B, b, p] 
{
  return %SparseMGCD[A, B] $
} $




/** 
 * @brief Computes GCD of A, B using Hensel Lifting
 * 
 * @param A, B are multivariate polynomials in Z[x, y1, ..., yn], 
 * with deg_x_(A) >= deg_x_(B)
 * 
 * @return G = GCD(A, B) 
 */
macro EZ_GCD[A_, B_]
{
  private _ALL $
  
  %get_union_vars[A_, B_, [vars]] $
  
  n = size(vars) $
  
  /* Treat special cases */
  if (n <= 1) then 
  {
    return %UGCD[A_, B_] $
    stop $ 
  }$
  
  if (A_ == 0) then 
  {
    return B_ $
    stop $ 
  }$
  
  if (B_ == 0) then 
  {
    return A_ $
    stop $ 
  }$

  x := vars[1] $
  dimvar coef_vars[1:n-1] $
  coef_vars := vars[2:n] $
  
  
  /* Be sure that degree requirement is fitted */
  if (puismax(B_, x) > puismax(A_,x)) then
  {
    return %EZ_GCD[B_, A_] $
    stop $
  }$
  
  /* Compute the content, primitive part, lcoeff GCD, ... viewing A and B as
   * polynomials in (Z[y1, ..., Yn])[x].
   */
  %primitive_cont_over_poly[A_, [A], [cont_A], [x], EZ_GCD] $ 
  %primitive_cont_over_poly[B_, [B], [cont_B], [x], EZ_GCD] $
     
  lcA = %lc[A, [x]] $
  lcB = %lc[B, [x]] $
  
  cont_G = %EZ_GCD[cont_A, cont_B] $
  
  g = %EZ_GCD[lcA, lcB] $
  
  /** Find a valid evaluation prime 
   */
    
  p = %choose_new_prime[lcA, lcB, [lcAmod], [lcBmod]] $

  /** Find a valid evaluation point b = (b1, ..., bn) with
   *  bi in Zp and many bi's set to zero as possible
   */
  
  b = %choose_eval_point[lcAmod, lcBmod, p,  [coef_vars], 0] $
    
  /** Calculate the image of A and B by the just-computed homomorphism, 
   * given by p and b
   */
 
  AI = Mod(A, p) $
  AI = coef_num(AI, (coef_vars, b)) $
  
  BI = Mod(B, p) $
  BI = coef_num(BI, (coef_vars, b)) $
  
  /** Compute the GCD of the images */
  GI = %UGCD[AI, BI] $
  d = puismax(GI, x) $
  
  force_new = 0$ // 1 iff the current homomorphism is known to be unlucky
  
  /** We know that the degree of the real gcd is less than d */
  if (d==0) then {return cont_G $ stop $} $
  
  /** Double check the answer, to remove possible unlucky homomorphisms 
    We want at least two evaluation with the same resulting degree before
    going further */
  
  while (0 == 0) do
  {
    p2 = %choose_new_prime[lcA, lcB, [lcAmod], [lcBmod]] $
    b2 = %choose_eval_point[lcAmod, lcBmod, p, [coef_vars], 1] $
    
    
    AI2 = Mod(A, p2) $
    AI2 = coef_num(AI2, (coef_vars, b2)) $
    
    BI2 = Mod(B, p2) $
    BI2 = coef_num(BI2, (coef_vars, b2)) $
  
    GI2 = %UGCD[AI2, BI2]$
    d2 = puismax(GI2, x) $
    
    if ((d==0) || (d2==0)) then {G = 1$ stop $} $ 
    
    if ((d2 < d) || (force_new == 1)) then {
      /* Previous homomorphism (I) was unlucky, so we store this homomorphism and repeat
       * the check step 
       */
        
      AI = AI2 $
      BI = BI2 $
      GI = GI2 $
      d = d2 $
      p = p2 $
      b = b2 $
      
      force_new = 0 $
      "Unlucky Homo"; 

    } else
    {
      if(d2 > d) then {
        /* This homomorphism (I2) is unlucky. Just drop it and repeat the check step */
        "Unlucky Homo";
      }
      else 
      {
        /* d2 = d, so we hope that I and I2 are good homomorphisms */
        
        /* Test for special case */
        if (d==0) then {G = 1$ stop $} $ 
          
        if (d == puismax(B, x)) then {
          div(A, B, q, r) $
          if (r == 0) then {
            G = B $
            stop $
          } else 
          {
            /* Else, the homomorphism is unlucky, so we choose another one with a lower
             degree */
            d = d - 1 $
            force_new = 1 $
            "Unlucky Homo";

          } $
        } else
        {
          /* Check for relatively prime cofactors */
          div(AI, GI, HI, r) $
          
          if(%UGCD[GI, HI] == 1) then {
            U = A $
            lcU = lcA $
          } 
          else
          {
            div(BI, GI, HI, r) $
            
            if(%UGCD[GI, HI] == 1) then {
              U = B $
              lcU = lcB $
            } 
            else
            {
              return %SGCD[A, B, b, p] $
            } $
          } $
                    
          /* Lifting step */ 
          try {
            %EZ_LIFT[U, GI, HI, p, b, [vars], lcU,  g, [G], [H]] $
            %primitive_cont_over_poly[G, [G], [trash], [x], EZ_GCD] $ 
          
            if(%div_test[A, B, G] == 1) then 
            {
              stop $
            }
            else
            {
              d = d - 1 $
              "Unlucky Homo";
              force_new = 1 $
            }$
          } 
          catch
          {
            d = d - 1 $
            "Unlucky Homo";
            force_new = 1 $
          } $
        }$
      }$
    }$
  }$
  return cont_G*G $
}$
