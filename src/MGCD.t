/** 
 * MGCD, a modular algorithm to compute GCD of multivariate polynomials over integers.
 * 
 * June 2016
 * 
 * Noe Brucy, ENS Rennes,
 * Intern at IMCCE, team ASD
 * Under supervision of Jacques Laskar and Mickael Gastineau
 */

/** 
 * @brief Computes GCD of A B in K[x, x1, ..., xn],
 * where K is a field (K  =  Fp)
 * 
 * K[x, x1, ..., xn] is seen as (K[x])[x1, ..., xn]
 * By evaluating A, B on x, we reduce the problem in K[x1, ..., xn] and solve
 * it recursively until the univariate case.
 * 
 * By doing it more than degx(GCD(A, B)), we are able to reconstruct the GCD of A and B 
 * via CRA interpolation. 
 * 
 * All computation are done in K.
 * 
 * @params A,B are polynomials in K[x, x1, ..., xn].
 * @param p is a prime large enough (p >> min(degxA, degxB)) 
 * 
 * @return the GCD G of A and B.
 */
macro PGCD[A, B, p]
{
  private _ALL$
  %get_union_vars[A, B, [vars]] $
  if (size(vars) <= 1) then
  {
    return  %UGCD[A, B] $ // univariate case
    stop$
  } $
  n = size(vars) $
  x := vars[sup(vars, 1)] $
  dimvar rem_vars[1: n - 1] $
  rem_vars := vars[1: n - 1] $
  
  %primitive_cont_over_poly[A, [Ap], [cont_A], [rem_vars]] $
  %primitive_cont_over_poly[B, [Bp], [cont_B], [rem_vars]] $
  
  cont_G = %UGCD[cont_A, cont_B] $ // content of G
  g = %UGCD[%lcoeff_over_poly[Ap, [rem_vars] ], %lcoeff_over_poly[Bp, [rem_vars]]] $
  // cont_G and g are in Fp[x]
  
  one = Mod(1, p) $/* Computing 1 in the right field */
  
  // Main loop
  q = one $ H = one $
  e = min(%total_degree_lc[Ap, [vars]], %total_degree_lc[Bp, [vars]]) + 1$
  limit =  min(puismax(Ap, x), puismax(Bp, x))  + puismax(g, x)$ // Bound for the degree in x of G
  n_iter = 0$ // number of non unlucky evaluation points so far 
  
  b = 0 $ // the evaluation point is chosen arbitrarily
  
  while (0 == 0) do 
  {
    b = b + 1 $
    gb = coef_num(g, (x, b))$
    
    while(gb == 0) do 
    {
      b = b + 1 $
      gb = coef_num(g, (x, b))$
    }$

    Ab = coef_num(Ap, (x, b)) $

    Bb = coef_num(Bp, (x, b)) $
    
    Cb = %PGCD[Ab, Bb, p] $ // Recursive call with n variables 
    
    d = %total_degree_lc[Cb, [vars]] $
    
    // Normalize Cb so that lcoeff(Cb) = gb
    Cb = gb * (1  / %lcoeff[Cb]) * Cb $
    
    mb = (Mod(x, p) - b) $

    // Test for unlucky homomorphism
    if (d < e) then 
    {
      q = mb $ 
      H = Cb $
      e = d $
      n_iter = 1 $
    } else 
    { 
      if(d == e) then
      {
        new_H = %PolyCRA[q, mb, H, Cb] $
        n_iter = n_iter + 1 $
        q = q * mb $

        n_iter$ limit$
        // Test for completion
        //H$ new_H$
        if ((H == new_H) || (n_iter > limit)) then 
        {
          if (%lcoeff_over_poly[H, [rem_vars]] == g) then 
          {
            %primitive_cont_over_poly[new_H, [G], [none], [rem_vars]] $ 
            
            if (%div_test[Ap, Bp, G] == 1) then
            {
              stop $
            }$
          }$
        }$
        H = new_H $
      }$
    }$ 
  }$
  return (cont_G * G) $
}$





/** 
 * @brief Computes GCD of A B (nonzero) by modular reduction
 * 
 * It computes the image of A, B in several finite field, 
 * then call PGCD to compute GCD of this images and reconstruct 
 * the GCD of A and B thanks to the Chinese Remainder Algorithm.
 * 
 * @params A,B are polynomials in Z[x1, ..., xn].
 * 
 * @return the GCD G of A and B.
 * 
 * As in Geddes1992 
 */

macro MGCD[A, B] 
{
  private _ALL$
  
  if (A == 0) then 
  {
    return B $
    stop $ 
  }$
  
  if (B == 0) then 
  {
    return A $
    stop $ 
  }$
  
  __coef_primitive_cont__(A, prim_A, cont_A) $
  __coef_primitive_cont__(B, prim_B, cont_B) $
  
  %get_union_vars[prim_A, prim_B, [vars]] $

  /*  We store the content of the GCD, and work with primitive polynomials */
  cont_G = __coef_gcd__(cont_A, cont_B) $
  
  /* Since the leading coefficient of G will be lost with evaluation,
   * we will NOT compute prim_G but a multiple H = c * prim_G of prim_G,
   * with c an integer. We know the leading coefficient of H, but c is unknown.
   * However, we know that prim_G = prim(H), which is enough to know prim_G
   */
  lc_H = __coef_gcd__(%lcoeff[prim_A], %lcoeff[prim_B]) $
  
  /* Bound of the degree on each variable */
  degree_bound = min(%max_degree[prim_A, [vars]], %max_degree[prim_B, [vars]]) $
  nb_vars = size(vars) $
  
  /* We store the coefficients of H in a dense representation. If polynomials
   * are sparse, use SparseMod instead
   */
  dim coef_H[1 : (degree_bound + 1)^nb_vars] $
  
  /* At each step, if previous homomorphisms were lucky, coef_H contains the
   * the coefficients of H modulo q
   */
  q = 1 $ 

  /* bound on the total degree of the leading term*/
  total_degree_lc_bound = min(%total_degree_lc[prim_A, [vars]], %total_degree_lc[prim_B, [vars]])$ 
  
  /* Initialiaztion of the unlucky homomorphism detector */
  last_deg = total_degree_lc_bound + 1$
  
  /* coefficients of H are bounded by */
  coef_bound =  2**total_degree_lc_bound * abs(lc_H) * min(__coef_norm_infinity__(prim_A), __coef_norm_infinity__(prim_B)) $
  
  /* Main loop. We need at least two successful evaluation to conclude */
  while (0 == 0) do 
  {
    p = %new_prime $
    
    /* Ensure that the homomorphism is not "bad" */
    while(lc_H mod p == 0) do 
    {
      p = %new_prime $
    }$
     
    /* Compute images */
    Ap = Mod(prim_A, p) $
    Bp = Mod(prim_B, p) $
    lc_Hp = Mod(lc_H, p) $
    
    /* Compute the GCD of the images*/
    Cp = %PGCD[Ap, Bp, p] $

    /* For the "unlucky homomorphism" verification */
    d = %total_degree_lc[Cp, [vars]] $
    
    /* Cp is normalized (we multiply it by the right constant in order that it has
     * the right leading coefficient, which is lc_H.
     * Then Cp is seen as a polynomial on Z
     */
    
    Cp = __coef_Zp_to_Z__(lc_H * (1 / %lcoeff[Cp]) * Cp) $
    
    if (d == 0) then 
    {
      /* In this case, there is no trick : we found it*/
      prim_G = 1 $
      stop$ 
    } $
    
    
    if (d < last_deg) then 
    {
      /* Two possibilities here :
       * 1. Cp is the first image :  initializate coef_H and proceed
       * 2. All previous homomorphisms were unlucky. Throw them and re-initializate
       *    H with Cp, which is the only one possibily not unlucky.
       */
      q = p $
      H = Cp $
      last_deg = d $
    } else
    {
      if (d == last_deg) then 
      { 
        /* Maybe Cp and the old images were not unlucky. Well ! 
         * Add Cp to H with CRA to reduce the distance to the bound
         */
        hvars = coef_tabvar(H) $
        coef_tabexp(H, tcH, texpH, hvars)$
        coef_tab(H, tcH, tmH, hvars)$
        
        texpH = vnumtodim(texpH) $ 
        
        for j = inf(tcH, 1) to sup(tcH, 1) 
        {
          tcH[j] = %IntegerCRA[q, p, tcH[j], coef_ext(Cp, (hvars, texpH[sup(texpH, 1) - j + 1,:]))] $
          /* May be a problem when p divides any coef of the GCD as we don't consider coeficients which are 0 
          * in H and nonzero in Cp ... */ 
        }$
        
        new_H = invcoef_tab(tcH, tmH)$
        q = q * p $
        
        /* Test for completion : it is time ? */
        if ((H == new_H) || (q > total_degree_lc_bound)) then 
        {
          /* Well, so as said before, prim_G is the primitive part of H */
          __coef_primitive_cont__(new_H, prim_G, cont_H) $
          
          /* test if prim_G is the GCD of prim_A and prim_B by division, which 
           * is enough because the were not "bad" homorphism
           */    
          if (%div_test[prim_A, prim_B, prim_G] == 1) then
          {
            stop $
          } else
          {
          "MGCD Failed div_test" ;
          H; new_H; Cp; 
          }$ 
        } $
        
        H = new_H $
      } else
      {
        "MGCD Bad Homo";
        H;Cp;
      }$
    }$
  } $
  return cont_G * prim_G $
} $

