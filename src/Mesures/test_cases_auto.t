/** 
 * Test cases to GCD computation of multivariate polynomials over integers.
 * 
 * May 2016
 * 
 * Noé Brucy, ENS Rennes,
 * Intern at IMCCE, team ASD
 * Under supervision of Jacques Laskar and Mickael Gastineau
 */


dim m[1:100, 1:3] $

for k = 1 to sup(m, 1) 
{
  m[k, 1] = 1 $ 
  m[k, 2] = 1 $
  m[k, 3] = 1 $
} $

/** My test cases */

o = x + y + x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + X_1 + X_2 + X_3 + X_4 + X_5$

m[1, 1] = (x - 1)*(x - 2)*(x - 3) $
m[1, 2] = (x - 1)*x $
m[1, 3] = (x - 1) $


m[2, 1] = (x - 1)*(x - 2)*x  $
m[2, 2] = (x - 2)*(x - 3)*(x - 4) $
m[2, 3] = x - 2 $

m[3, 1] = x*(x - 1)*(x - 2)*(x - 3)  $
m[3, 2] = (x - 3)*(x - 4)*(x - 5)*(x - 6)*(x - 7) $
m[3, 3] = x - 3 $


m_d4 = x - 9$
m_f4 = x^3 - 3*x + 2 $
m_g4 = 2*x^2 - 1$

m_a4 = m_d4 * m_f4 $
m_b4 = m_d4 * m_g4 $

m[4, 1] =  m_a4  $
m[4, 2] =  m_b4  $
m[4, 3] =  m_d4  $


m[5, 1] =  104*X_3**2
 + 16*X_3**3
 - 78*X_2*X_3
 + 131*X_2*X_3**2
 + 22*X_2*X_3**3
 + 275*X_2**2*X_3
 + 30*X_2**2*X_3**2
 - 60*X_2**3
 + 110*X_2**3*X_3
 + 150*X_2**4
 - 78*X_1*X_3
 - 52*X_1*X_3**2
 - 26*X_1*X_2*X_3
 - 55*X_1*X_2*X_3**2
 - 18*X_1*X_2**2
 - 152*X_1*X_2**2*X_3
 - 105*X_1*X_2**3
 + 30*X_1**2*X_3
 + 42*X_1**2*X_2 $

m[5, 2] =  26*X_2*X_3**2
 + 4*X_2*X_3**3
 + 182*X_2**2*X_3
 + 28*X_2**2*X_3**2
 + 20*X_2**3*X_3
 + 140*X_2**4
 + 104*X_1*X_3
 + 81*X_1*X_3**2
 + 10*X_1*X_3**3
 + 13*X_1*X_2*X_3
 - 8*X_1*X_2*X_3**2
 + 80*X_1*X_2**2
 - 34*X_1*X_2**2*X_3
 - 88*X_1*X_2**3
 - 40*X_1**2*X_3
 - 25*X_1**2*X_3**2
 - 56*X_1**2*X_2
 - 40*X_1**2*X_2*X_3
 - 7*X_1**2*X_2**2  $
 
m[5, 3] = - 13*X_3
 - 2*X_3**2
 - 10*X_2**2
 + 5*X_1*X_3
 + 7*X_1*X_2
  $
  
m[6, 1] = 
   104*X_3
 + 81*X_3**2
 + 10*X_3**3
 + 13*X_2*X_3
 - 8*X_2*X_3**2
 + 80*X_2**2
 - 34*X_2**2*X_3
 - 88*X_2**3 $

m[6, 2] = 
 - 40*X_3
 - 25*X_3**2
 - 56*X_2
 - 40*X_2*X_3
 - 7*X_2**2 $
 
m[6, 3] = 1 $


m[7, 1]  = 
   40*X_1*X_3
 - 30*X_1*X_3**2
 - 16*X_1*X_2
 + 43*X_1*X_2*X_3
 - 42*X_1*X_2*X_3**2
 + 10*X_1*X_2**2
 - 35*X_1*X_2**2*X_3
 - 5*X_1**2*X_3
 - 30*X_1**2*X_3**2
 - 30*X_1**2*X_2
 - 52*X_1**2*X_2*X_3
 + 30*X_1**2*X_2**2
 - 45*X_1**3*X_3
 + 54*X_1**3*X_2
$

m[7, 2] = 
   20*X_3**3
 - 8*X_2*X_3**2
 + 28*X_2*X_3**3
 + 5*X_2**2*X_3
 - 2*X_2**3
 + 7*X_2**3*X_3
 + 10*X_1*X_3
 + 15*X_1*X_3**2
 + 20*X_1*X_3**3
 - 4*X_1*X_2
 - 17*X_1*X_2*X_3
 - 3*X_1*X_2*X_3**2
 + 10*X_1*X_2**2
 - 30*X_1*X_2**2*X_3
 - 6*X_1*X_2**3
 + 10*X_1**2*X_3
 + 15*X_1**2*X_3**2
 - 12*X_1**2*X_2
 - 43*X_1**2*X_2*X_3
 + 30*X_1**2*X_2**2
 $

m[7, 3] = 
   5*X_3
 - 2*X_2
 + 7*X_2*X_3
 + 5*X_1*X_3
 - 6*X_1*X_2 $



m_a8 = 54
 - 225*x_1
 - 315*x_1*x_2
 + 225*x_1**2
 + 147*x_2
 + 98*x_2**2
$

m_b8 =  - 18
   + 117*x_1
 + 294*x_1*x_2
 - 180*x_1**2
 - 105*x_2
 - 98*x_2**2
$





m_a9  = 
   78189804
 - 185703*x_2
 + 98*x_2**2
 $

m_b9 = 
 - 62588988
 + 173355*x_2
 - 98*x_2**2
 $
 
m_f10 = 4 + x1^2 + x2^2$
m_g10 = 2 + x1 + x2 $
m_d10 = 3*x1*x2 + x1 $

m_f11 = 4 + x1^2 + x2^2 + x3^8$
m_g11 = 2 + x1 + x2*x3^4 + x3 $
m_d11 = 3*x1*x2 + x1 + 3*x1*x3  $

m_d12 = x1*x2^2 - 5*x2$
m_f12 = x1*x2 + 5*x1 $
m_g12 = x1 + 1$

m_d13 = (4*y^2+2*z)*x^2 + (10*y^2+6*z) $
m_f13 = 4*x +z*x^2 + z^4*y*x^3 $
m_g13 = 3*x*y*z + 9*z^4*y*x^2 + 45*z^2*y^5*x^3 $

m_a13 = m_d13 * m_f13$
m_b13 = m_d13 * m_g13$

m_d14 = (y + 50)*x^3 + 100*y$
m_a14 = (x - y + 1)*m_d14 $
m_b14 = (x + y + 1)*m_d14 $

m_d15 = (3*y^2 - 90)*x^3 + 12*y + 100 $
m_a15 = (x - y + 1)*m_d15 $
m_b15 = (x + y + 1)*m_d15 $


m_d16 = (y + 2)*x^3 + 12*y^2 + 24*y$
m[16, 1] = (x - y + 1)*m_d16 $
m[16, 2] = (x + y + 1)*m_d16 $
m[16, 3] = m_d16 $


m[17, 1] = 
   72*X_1*X_2**3*X_3**5
 + 8*X_1*X_2**7*X_3**2
 + 45*X_1**2*X_2*X_3**5
 - 9*X_1**2*X_2**2*X_3**5
 - 32*X_1**2*X_2**3*X_3**2
 + 5*X_1**2*X_2**5*X_3**2
 - 1*X_1**2*X_2**6*X_3**2
 + 45*X_1**3*X_3**4
 - 20*X_1**3*X_2*X_3**2
 + 4*X_1**3*X_2**2*X_3**2
 + 5*X_1**3*X_2**4*X_3
 - 20*X_1**4*X_3
 - 8*X_1**5*X_2**4*X_3**3
 + 81*X_1**6*X_3**3
 - 5*X_1**6*X_2**2*X_3**3
 + 1*X_1**6*X_2**3*X_3**3
 + 9*X_1**6*X_2**4
 - 36*X_1**7
 - 5*X_1**7*X_2*X_3**2
 - 9*X_1**10*X_2*X_3 $

m[17, 2]  = 
 - 32*X_1*X_2**3*X_3**7
 - 20*X_1**2*X_2*X_3**7
 + 4*X_1**2*X_2**2*X_3**7
 + 88*X_1**2*X_2**5*X_3**2
 - 20*X_1**3*X_3**6
 + 55*X_1**3*X_2**3*X_3**2
 - 128*X_1**3*X_2**3*X_3**6
 - 11*X_1**3*X_2**4*X_3**2
 + 32*X_1**3*X_2**4*X_3**5
 - 80*X_1**4*X_2*X_3**6
 + 55*X_1**4*X_2**2*X_3
 + 20*X_1**4*X_2**2*X_3**5
 + 16*X_1**4*X_2**2*X_3**6
 - 4*X_1**4*X_2**3*X_3**5
 - 112*X_1**4*X_2**6*X_3**2
 - 80*X_1**5*X_3**5
 + 20*X_1**5*X_2*X_3**4
 - 70*X_1**5*X_2**4*X_3**2
 + 14*X_1**5*X_2**5*X_3**2
 - 36*X_1**6*X_3**5
 - 70*X_1**6*X_2**3*X_3
 + 99*X_1**7*X_2**2
 - 144*X_1**8*X_3**4
 + 36*X_1**8*X_2*X_3**3
 - 126*X_1**9*X_2**3 $
 
m[17, 3]  = 8*X_1*X_2**3*X_3**2
 + 5*X_1**2*X_2*X_3**2
 - 1*X_1**2*X_2**2*X_3**2
 + 5*X_1**3*X_3
 + 9*X_1**6
$


m_a20 = x^8 + (y + y^5)*x^6 + 3*(y^2 + 1)*x^4 + y*x^3 + 4*y$ 
m_b20 = 3*y*x^6 + y^2*x^2 + (5*y + 3)*x + 7$ 


m_a21 = x^4  + 4*x^2*y + 12*y + 3$ 
m_b21 = (3 + y^2)*x^3 + (5*y + 3)*x + 7$ 

m[22, 1] = 
   3
 + 1*y**2
$

m[22, 2] = 
   27
 + 108*y
 + 18*y**2
 + 72*y**3
 + 3*y**4
 + 12*y**5
$

m[22, 3] = 3 + y^2 $
  

m_a23  = 1*x3**2*x6
 + 1*x3**4*x5 $

m_b23 = 
   1*x3*x6**2
 + 1*x3**2*x4**2*x5**2
 + 1*x3**2*x4**2*x6*x5**4
 + 1*x3**3*x6*x5 $

 
m[24, 1] = 
 - 5*X_3
 - 6*X_3**2
 - 1*X_3**3
 - 5*X_3**4
 - 20*X_2
 - 24*X_2*X_3
 - 19*X_2*X_3**2
 - 31*X_2*X_3**3
 - 2*X_2*X_3**4
 - 10*X_2*X_3**5
 - 5*X_2**2
 - 11*X_2**2*X_3
 + 23*X_2**2*X_3**2
 - 12*X_2**2*X_3**3
 + 17*X_2**2*X_3**4
 + 35*X_2**3
 + 47*X_2**3*X_3
 + 23*X_2**3*X_3**2
 + 36*X_2**3*X_3**3
 + 8*X_2**4
 + 37*X_2**4*X_3
 - 9*X_2**4*X_3**2
 + 2*X_2**5
 - 20*X_2**5*X_3
 - 14*X_2**6
 + 7*X_1*X_3
 + 22*X_1*X_3**2
 + 27*X_1*X_3**3
 + 4*X_1*X_3**4
 + 20*X_1*X_3**5
 + 53*X_1*X_2
 + 3*X_1*X_2*X_3
 - 14*X_1*X_2*X_3**2
 + 42*X_1*X_2*X_3**3
 - 33*X_1*X_2*X_3**4
 - 28*X_1*X_2**2
 - 48*X_1*X_2**2*X_3
 - 50*X_1*X_2**2*X_3**2
 - 51*X_1*X_2**2*X_3**3
 - 49*X_1*X_2**3
 - 62*X_1*X_2**3*X_3
 + 1*X_1*X_2**3*X_3**2
 - 10*X_1*X_2**4
 + 49*X_1*X_2**4*X_3
 + 14*X_1*X_2**5
 - 15*X_1**2
 - 35*X_1**2*X_3
 - 55*X_1**2*X_3**2
 - 28*X_1**2*X_3**3
 - 37*X_1**2*X_3**4
 - 3*X_1**2*X_2
 + 48*X_1**2*X_2*X_3
 - 7*X_1**2*X_2*X_3**2
 + 50*X_1**2*X_2*X_3**3
 + 57*X_1**2*X_2**2
 + 17*X_1**2*X_2**2*X_3
 - 8*X_1**2*X_2**2*X_3**2
 - 50*X_1**2*X_2**3
 - 53*X_1**2*X_2**3*X_3
 + 16*X_1**3
 + 37*X_1**3*X_3
 - 14*X_1**3*X_3**2
 - 14*X_1**3*X_3**3
 - 32*X_1**3*X_2
 + 12*X_1**3*X_2*X_3
 + 32*X_1**3*X_2*X_3**2
 + 58*X_1**3*X_2**2
 + 38*X_1**3*X_2**2*X_3
 - 12*X_1**3*X_2**3
 + 31*X_1**4
 + 60*X_1**4*X_3
 + 25*X_1**4*X_3**2
 - 10*X_1**4*X_2
 + 11*X_1**4*X_2*X_3
 + 14*X_1**4*X_2**2
 + 14*X_1**5
 + 16*X_1**5*X_3
 + 2*X_1**6
$

m[24, 2] = 
   25*X_3**2
 + 30*X_3**3
 + 5*X_3**4
 + 25*X_3**5
 + 10*X_2
 - 3*X_2*X_3
 + 14*X_2*X_3**2
 + 68*X_2*X_3**3
 - 14*X_2*X_3**4
 + 30*X_2*X_3**5
 - 15*X_2**2
 + 2*X_2**2*X_3
 - 8*X_2**2*X_3**2
 + 10*X_2**2*X_3**3
 + 4*X_2**2*X_3**4
 - 19*X_2**3*X_3
 + 9*X_2**3*X_3**2
 - 14*X_2**3*X_3**3
 - 4*X_2**4
 + 12*X_2**4*X_3
 - 16*X_2**4*X_3**2
 + 6*X_2**5
 - 4*X_2**5*X_3
 + 25*X_1
 + 30*X_1*X_3
 - 45*X_1*X_3**2
 - 3*X_1*X_3**3
 - 18*X_1*X_3**4
 - 15*X_1*X_3**5
 + 1*X_1*X_2
 + 70*X_1*X_2*X_3
 - 32*X_1*X_2*X_3**2
 + 14*X_1*X_2*X_3**3
 - 5*X_1*X_2*X_3**4
 + 46*X_1*X_2**2
 + 33*X_1*X_2**2*X_3
 + 8*X_1*X_2**2*X_3**2
 + 41*X_1*X_2**2*X_3**3
 - 10*X_1*X_2**3
 + 10*X_1*X_2**3*X_3
 + 3*X_1*X_2**3*X_3**2
 - 6*X_1*X_2**4
 - 14*X_1*X_2**4*X_3
 - 10*X_1*X_2**5
 - 40*X_1**2
 - 56*X_1**2*X_3
 - 83*X_1**2*X_3**2
 - 37*X_1**2*X_3**3
 - 31*X_1**2*X_3**4
 - 67*X_1**2*X_2
 - 34*X_1**2*X_2*X_3
 - 88*X_1**2*X_2*X_3**2
 - 73*X_1**2*X_2*X_3**3
 - 11*X_1**2*X_2**2
 - 27*X_1**2*X_2**2*X_3
 + 1*X_1**2*X_2**2*X_3**2
 + 2*X_1**2*X_2**3
 + 43*X_1**2*X_2**3*X_3
 + 12*X_1**2*X_2**4
 - 63*X_1**3
 - 8*X_1**3*X_3
 + 27*X_1**3*X_3**2
 + 12*X_1**3*X_3**3
 + 14*X_1**3*X_2
 - 49*X_1**3*X_2*X_3
 - 24*X_1**3*X_2*X_3**2
 - 34*X_1**3*X_2**2
 - 40*X_1**3*X_2**2*X_3
 + 12*X_1**3*X_2**3
 + 40*X_1**4
 + 82*X_1**4*X_3
 + 72*X_1**4*X_3**2
 + 42*X_1**4*X_2
 + 14*X_1**4*X_2*X_3
 - 10*X_1**4*X_2**2
 + 50*X_1**5
 + 52*X_1**5*X_3
 + 12*X_1**5*X_2
$

m[24, 3] = 1 $

m[25, 1] = 0 $
m[25, 2] = 1 $
m[25, 3] = 1 $

m[25, 1] = x1 + x2 +x3 +x4 +x5 +x6  $
m[25, 2] = x1 + x2 +x3 +x4 +x5 +x6  $
m[25, 3] = x1 + x2 +x3 +x4 +x5 +x6  $


m[25, 1] = x3 * (x1 + x2 + x3 + x4 +x5 +x6)  $
m[25, 2] = x3 *(x7 + x2 +x3 +x4 +x5 + x6)  $
m[25, 3] = x3 $

m[26, 1] = x^3 + x $
m[26, 2] = x^3 + 4*x^2 + x + 4$
m[26, 3] = x^2 + 1$


/** Zippel Tests cases (See Zippel1979)*/

dim zipp[1:20, 1:3] $

for k = 1 to sup(zipp, 1) 
{
  zipp[k, 1] = 1 $ 
  zipp[k, 2] = 1 $
  zipp[k, 3] = 1 $
} $

zipp_d1 = x1^2 + x1 +3$

zipp_f1 = 2*x1^2 + 2*x1 + 1$
zipp_g1 = x1^2 + 2*x1 + 2$

zipp[1,1] = zipp_d1 * zipp_f1$
zipp[1,2] = zipp_d1 * zipp_g1$
zipp[1,3] = zipp_d1 $

zipp_d2 = 2*x1^2*x2^2 + x1*x2 + 2*x1$

zipp_f2 = x2^2 + 2*x1^2*x2 + x1^2 + 1$
zipp_g2 = x1^2*x2^2 + x1^2*x2 + x1*x2 + x1^2 + x1$

zipp[2,1] = zipp_d2 * zipp_f2$
zipp[2,2] = zipp_d2 * zipp_g2$
zipp[2,3] = zipp_d2 $



zipp_d6 = x1*x2^2*x4^2*x6^2 + x1*x2^2*x3^2*x4*x5^2*x6^2 + x1^2*x3*x6^2 + x1^2*x2*x3^2*x4*x5^2*x6 +x1^2*x2*x3^2*x4 $
zipp_f6 = x1^2*x2*x4*x5^2*x6^2 + x1*x3*x5^2*x6^2 + x1^2*x2^2*x6^2 + x1^2*x2^2*x3^2*x5*x6 +x1*x3^2*x4*x5 $
zipp_g6 = x2^2*x3^2*x4*x5^2*x6 + x1*x4^2*x5*x6 + x2^2*x3^2*x4*x5*x6 + x1*x2^2*x3*x4^2*x6 +x1^2*x3*x5^2 $

zipp[6,1] = zipp_d6 * zipp_f6$
zipp[6,2] = zipp_d6 * zipp_g6$
zipp[6,3] = zipp_d6 $



/** Geddes exercices and exemples (See Geddes1992, chapter 6)*/

gex6_a5 = x^3 + 10*x^2 -432*x +5040 $
gex6_u5 = Mod(x, 5) $
gex6_w5 = Mod(x**2 - 2, 5) $


gex6_a7 = 12*x^3 + 10*x^2 -36*x + 35 $
gex6_u7 = Mod(2*x, 5) $
gex6_w7 = Mod(x**2 + 2, 5) $

gex6_a7m = (2*x + 5*y)*(6*x^2 - 2*(y + 4)*x + 10*y - 3) $
gex6_u7m = 2*x + 5 $
gex6_w7m = 6*x^2 - 10*x + 7$
gex6_Jm = [0 : 1] $
gex6_lc1m = 2 $
gex6_lc2m = 6 $

gex6_a7m2 = (2*x + z^2 + 5*y -1)*(3*(z+1)*x^2 - 2*(y + 4)*x + 10*y - 3*z^2 + z*y - 1) $
gex6_u7m2 = 2*x + 5 $
gex6_w7m2 = 6*x^2 - 10*x + 7$
gex6_Jm2 = [0 : 1 : 1] $
gex6_lc1m2 = 2 $
gex6_lc2m2 = 3*(z + 1) $
 

/** Geddes exercices and exemples (See Geddes1992, chapter 7)*/

gex_a2 = x^8 + x^6 -3*x^4 -3*x^3 + 8*x^2 +2*x - 5$
gex_b2 = 3*x^6 + 5*x^4 - 4*x^2 - 9*x + 21$


gex_a16 = 9*x^5 +2*x^4*y*z -189*x^3*y^3*z + 117*x^3*y*z^2 + 3*x^3 -42*x^2*y^4*z^2 +26*x^2*y^2*z^3 
	  +18*x^2 - 63*x*y^3*z + 39*x*y*z^2 + 4*x*y*z + 6$
gex_b16 = 6*x^6 - 126*x^4*y^3*z + 78*x^4*y*z^2 +x^4*y + x^4*z + 13*x^3 -21*x^2*y^4*z - 21*x^2*y^3*z^2
          + 13*x^2*y^2*z^2 + 13*x^2*y*z^3 -21*x*y^3*z + 13*x*y*z^2 +2*x*y +2*x*z + 2 $

gex_a22 = x^3 - 3*x^2 + 2*x$
gex_b22 = x^3 + 6*x^2 + 11*x + 6$


gexo_a1 = 2*x^6 +5*x^5 +7*x^4 + 3*x^3 + 6*x^2 - 2*x +1$
gexo_b1 = 3*x^5 +3*x^4 + 6*x^3 - x^2 + 3*x - 4$

gexo_a2 = x^4 + x^3 - w$
gexo_b2 = x^3 + 2*x^2 + 3*w*x - w + 1$


gexo_a14a = 1206*x^10 + 1413*x^9 - 1201*x^8 + 2506*x^7 + 4339*x^6 + 12*x^5 + 1405*x^4 + 415*x^3 + 1907*x^2 - 588*x - 1818$
gexo_b14a =  402*x^9 + 203*x^8 + 402*x^7 + 103*x^6 - 704*x^5 + 1706*x^4 - 1196*x^3 - 313*x^2 + 710*x - 383$
