include("includes.t")$

/*
 * Exemple of bench : 
 * %bench_gen["degregcd", 2, 3, -1, 0, -1, 20, time_GCDHEU_int, "GCDHEU", time_maple, "Maple"];
 * 
 */

SEEDREF = 0;


/*Nb of sample by set of parameters */
NB_SAMPLE = 5; 

/**
 * Set the seed for random generation
 * 
 * @param seedR the seed
 * 
 * @return the actual seed
 */
macro set_seed[seedR]
{
  maple_put(seedR)$
  maple;
    seed := randomize(seedR):
  end;
  seed =0$
  maple_get(seed) $
  return seed $
}$

%set_seed[SEEDREF] $

macro mean[t] 
{
  private _ALL $
  s = sum k = inf(t, 1) to sup(t, 1) 
  {
    return t[k] $
  } $
  return s / size(t) $
} $

macro std_dev[t, mean] 
{ 
  private _ALL $
   
  s = sum k = inf(t, 1) to sup(t, 1) 
  {
    return (t[k] - mean)**2 $
  } $
  return sqrt(s / size(t)) $
} $

macro time_maple[f]
{
  private _ALL$
  maple_get(ttfindreal); 
  return ttfindreal;
} $

macro time_maple_with_transfert[f]
{
  private _ALL$
  
  "Launching Maple transfert";
  time_s$
  for j=1 to 2 {maple_put(f[j])$}$
  maple $
    gm:=gcd(f[1],f[2]):
  end$
  maple_get(gmaple)$
  time_t(u,r)$
  return r ;
} $

macro time_GCDHEU_comp[f]
{
  private _ALL$
  A=f[1] $
  B=f[2] $
  saveF(A) $
  saveF(B) $
  "Launching GCDHEU Comp";
  ! "../../GCDHEU/driver";
  loadF(G)$ 
  Gm = f[3] $
  saveF(Gm) $
  delete(Gm) $
  loadF(Gm) $
  if (((G-Gm)!=0) && ((G+Gm)!=0)) then  {f[1]; f[2]; Gm; G; err = G-Gm; error("Wrong result\n");};
  vnumR r $ vnumR u$
  read("temps_gcd.dat", r, u) $
  return r[1]$
} $

macro time_check[f, gcd_meth]
{
  private _ALL$
  time_s$ gt=%gcd_meth[f[1],f[2]]$ time_t(u,r)$
  if (((gt-f[3])!=0) && ((gt+f[3])!=0)) then  {f[1]; f[2]; f[3]; gt; error("Wrong result\n");};
  return r;
} $

macro time_GCDHEU_int[f]
{
  private _ALL$
  "Launching GCDHEU int";
  return %time_check[f, [GCD]];
} $

macro time_MGCD[f]
{
  private _ALL$
  "Launching MGCD";
  return %time_check[f, [MGCD]];
} $

macro time_SparseMGCD[f]
{
  private _ALL$
  "Launching SparseMGCD";
  return %time_check[f, [SparseMGCD]];
} $

macro time_EZ_GCD[f]
{
  private _ALL$
  "Launching EZ_GCD";
  return %time_check[f, [EZ_GCD]];
} $

macro time_euclidian_PRS_GCD[f]
{
  private _ALL$
  "Launching euclidian_PRS_GCD";
  return %time_check[f, [euclidian_PRS_GCD]];
} $

macro time_primitive_PRS_GCD[f]
{
  private _ALL$
  "Launching primitive_PRS_GCD";
  return %time_check[f, [primitive_PRS_GCD]];
} $

macro time_reduced_PRS_GCD[f]
{
  private _ALL$
  "Launching reduced_PRS_GCD";
  return %time_check[f, [reduced_PRS_GCD]];
} $

macro time_subresultant_PRS_GCD[f]
{
  private _ALL$
  "Launching subresultant_PRS_GCD";
  return %time_check[f, [subresultant_PRS_GCD]];
} $

/** nbterms = 0 -> dense */
macro sample[nbvars, degregcd, degrecof, nbterms, scoef] 
{
  msg("%d vars, degree GCD =  %d, degree COF = %d, size = %d, terms = %d", nbvars, degregcd, degrecof, scoef, nbterms);
  maple_put(nbvars)$
  dimvar X[1:nbvars]$
  tabvar(X)$
  s_vars=X*1$
  maple_put(s_vars);
  maple_put(degregcd)$
  maple_put(degrecof)$
  maple_put(scoef)$
  maple_put(nbterms)$
  "";
  "Lauching Maple"; 
  if(nbterms > 0) then {
    maple $
      c:=randpoly(convert(s_vars, list), degree=degregcd, coeffs=rand(-2**scoef..2**scoef), terms=nbterms):
      for s to 2 do cof[s]:= randpoly(convert(s_vars, list),degree=degrecof, coeffs=rand(-2**scoef..2**scoef), terms=nbterms) od 
    end $
  } else {
    maple $
      c:=randpoly(convert(s_vars, list), degree=degregcd, coeffs=rand(-2**scoef..2**scoef), dense):
      for s to 2 do cof[s]:= randpoly(convert(s_vars, list),degree=degrecof, coeffs=rand(-2**scoef..2**scoef), dense) od 
    end $
  } $
  maple $
    for s to 2 do f[s] := expand(c*cof[s]) od
    ttcpu:=time():
    ttreal:=time[real]():  
    gmaple:=gcd(f[1],f[2]):
    ttfindreal:=time[real]()-ttreal;
    ttfindcpu:=time()-ttcpu:
  end$
  "Receiving GCD";

  maple_get(gmaple)$
  maple_get(c)$
  dim cof[1:2], f[1:3]$
  "Receiving cofactors";
  for j=1 to 2 {j; maple_get(cof[j])$}$
  "Rebuilding problem";
  for j=1 to 2 {j; f[j] =  c * cof[j] $}$
    
  f[3] = gmaple $
  return f $
} $

/** 
 * @brief Compares differents methods on same polynomes
 * 
 * @param on_x : a string (nbvars, degregcd, degrecof, nbterms or scoef). 
 * The macro will runs GCD methods for several values of the represented parameter x
 * and will plot the graph time = f(x).
 * 
 * @params nbvars, degregcd, degrecof, nbterms, scoef : set fixed parameters and 
 * the initial value for the parameter on the x axis. If a parameter is set to -1
 * it will have the same value that the parameter of the x_axis.
 * If nbterms = 0, polynomials will be dense.
 *
 * @param x_max the maximal value for the parameter on the x axis
 * 
 * @param ... : methods that will be used for the benchmark and their corrresponding title.
 * A method is a macro taking a tab of three polynomials as parameter and returning 
 * the real time spent by the algorithm.
 * 
 * Exemple of call :
 *  %bench_gen["degregcd", 2, 3, -1, 0, -1, 20, time_GCDHEU_int, "GCDHEU", time_maple, "Maple"];
 * 
 * @return the title of the bench. Data is stored in title.dat and a plot is done
 * on title.pdf. 
 */
macro bench_gen[on_x, nbvars, degregcd, degrecof, nbterms, scoef, x_max, ...] 
{ 
  private _ALL $
  public NB_SAMPLE $
  
  nbmeth = int(size(macro_optargs)  / 2 ) $
  dimvar para[1:5]$
  
  tabvar(para);
  
  para[1] = nbvars $ 
  para[2] = degregcd $
  para[3] = degrecof $
  para[4] = nbterms $
  para[5] = scoef $
  
  var = 0$

  switch (on_x) 
  {
    case "nbvars" :
    {
      title =  "v" + str(nbvars) + "-" + str(x_max) + "dg" + str(degregcd) + "dc" + str(degrecof)  + "t" + str(nbterms) + "s" + str(scoef) $
      var = 1 $
    }
    case "degregcd" : 
    { 
      title =  "v" + str(nbvars)  + "dg" + str(degregcd) + "-" + str(x_max) + "dc" + str(degrecof)  + "t" + str(nbterms) + "s" + str(scoef)  $
      var = 2 $
    }
    case "degrecof" : 
    {
      title =  "v" + str(nbvars) + "dg" + str(degregcd) + "dc" + str(degrecof) + "-" + str(x_max)  + "t" + str(nbterms) + "s" + str(scoef) $
      var = 3 $
    }
    case "nbterms" :
    {
      title =  "v" + str(nbvars) + "dg" + str(degregcd) + "dc" + str(degrecof)  + "t" + str(nbterms)  + "-" + str(x_max) + "s" + str(scoef)  $
      var = 4 $
    }
    case "scoef" : 
    {
      title =  "v" + str(nbvars) + "dg" + str(degregcd) + "dc" + str(degrecof)  + "t" + str(nbterms)  + "s" + str(scoef) + "-" + str(x_max) $
      var = 5 $
    }
    else 
    {
      error("Choose suitable value for abcissa : nbvars, degregcd, degrecof, nbterms, scoef");
    }
  }$
  
  for j = 1 to 5 {
    if (para[j] == -1) then
    {
      para[j] := para[var]$
    }$
  }$
  
  vnumR v_x$ /* x values */
  dim v_y[1:nbmeth]$ /* y values */
  dim delta_y[1:nbmeth]$ /*y uncertainty */

  dim titles[1:nbmeth]$
  dimvar meths[1:nbmeth]$
  dim cur_y[1:nbmeth, 1:NB_SAMPLE] $ /* to store y for same kind of sample */
  
  for j = 1 to nbmeth {
    
    vnumR v_y_meth $
    vnumR delta_y_meth $
    
    v_y[j] = v_y_meth $
    delta_y[j] = delta_y_meth $
    
    meths[j] := macro_optargs[2*j - 1] $
    title;
    macro_optargs[2*j];
    titles[j] = macro_optargs[2*j] + "_" $
  }$
  
  while (para[var] <= x_max) do 
  {
    v_x = vnumR[v_x:para[var]]$
    
    for k = 1 to NB_SAMPLE 
    {
      dim h[1:3]$
      h = %sample[para[1], para[2], para[3], para[4], para[5]] $
      for j = 1 to nbmeth {
        meth := meths[j] $
        y = %meth[h] $
        cur_y[j, k] = y $
      }$
    }$
  
    
    for j = 1 to nbmeth 
    {
      m_y = %mean[cur_y[j,:]]$
      d_y = %std_dev[cur_y[j,:], m_y] $
      v_y[j] = vnumR[v_y[j]:m_y] $
      delta_y[j] = vnumR[delta_y[j]:d_y] $
    }$
    
    write(title + ".dat", v_x, v_y) $
    para[var] = para[var] + 1$
  }$
  
  
  /* Plot with error bars */
    
  dim by_meth [1:2*nbmeth] $
  for j = 1 to nbmeth 
  {
      by_meth[2*j - 1] = v_y[j]$
      by_meth[2*j] = delta_y[j]$
  }$
  
  title_err= title + "_with_err.dat";
  write(title_err, v_x, by_meth) $
  
  plotps title +  ".pdf";
  gnuplot;
    set term pdf color
    set xlabel '@on_x@'
    set ylabel "time"
    set title '@title@
    set key outside
    set logscale y
    set grid
  end;
  
  t_act = titles[1]$
  gnuplot;
    plot '@title_err@' using 1:2:3 title '@t_act@' with errorbars ps 0.1;
  end;
  
  for j = 2 to nbmeth {
    t_act = titles[j]$
    jy = str(2*j) $
    jerr = str(2*j + 1) $
    
    gnuplot;
      replot '@title_err@' using 1:@jy@:@jerr@ title '@t_act@' with errorbars ps 0.1;
    end;
  }$
  
  gnuplot;
    unset logscale y
  end;
  
  plotps_end;
  
  return title $
}$

macro plot_ratio[on_x, nbvars, degregcd, degrecof, nbterms, scoef, x_max, meth1, title1, meth2, title2] {
  private _ALL$
  title_bench = %bench_gen[on_x, nbvars, degregcd, degrecof, nbterms, scoef, x_max, meth1, title1, meth2, title2] $
  title_ratio = "ratio_" + title1 + "over" + title2 + "_" + title_bench $
  vnumR v_x$
  
  dim v_y[1:2]$
  vnumR v_y1 $ 
  vnumR v_y2 $ 
  v_y[1] = v_y1 $
  v_y[2] = v_y2 $
  read(title_bench + ".dat", v_x, v_y) $
  ratio = v_y[1] / v_y[2] $
  write( title_ratio + ".dat", v_x, ratio) $
  
  plotps title_ratio +  ".pdf";
  gnuplot;
  set term pdf color
  set grid
  end;
  
  plot("set xlabel """ + title_bench + """; set ylabel ""time(" + title1 +") /  time(" + title2 +")"" ", (v_x, ratio, "title '" + title1 + " over " + title2  + "'")) ;
  
  gnuplot;
  unset grid
  end;
  
  return title_ratio $
  plotps_end;
}$


macro grid_ratio[on_x, on_y, nbvars, degregcd, degrecof, nbterms, scoef, x_max, y_max, meth1, title1, meth2, title2] 
{
  private _ALL$
  dimvar para[1:5]$
  
  tabvar(para);
  
  para[1] = nbvars $ 
  para[2] = degregcd $
  para[3] = degrecof $
  para[4] = nbterms $
  para[5] = scoef $
  
  var = 0$

  switch (on_x) 
  {
    case "nbvars" :
    {
      title_y =  "xv" + str(nbvars) + "-" + str(y_max) $
      var = 1 $
    }
    case "degregcd" : 
    { 
      title_y =  "xdg" + str(degregcd) + "-" + str(y_max)  $
      var = 2 $
    }
    case "degrecof" : 
    {
      title_y =  "xdc" + str(degrecof) + "-" + str(y_max)  $
      var = 3 $
    }
    case "nbterms" :
    {
      title_y =  "xt" + str(nbterms)  + "-" + str(y_max)  $
      var = 4 $
    }
    case "scoef" : 
    {
      title_y =  "xs" + str(scoef) + "-" + str(y_max) $
      var = 5 $
    }
    else 
    {
      error("Choissisez une valeur correcte : nbvars, degregcd, degrecof, nbterms, scoef");
    }
  }$
  
  
  dim columns[para[var]:x_max]$
  vnumR v_x, ratio, v_y$
  
  while (para[var] <= x_max) do 
  {
    title_ratio = %plot_ratio[on_y, para[1], para[2], para[3], para[4], para[5], y_max, meth1, title1, meth2, title2] $
    
    read( title_ratio + ".dat", v_y, ratio) $
    columns[para[var]] = ratio$
    
    v_x =vnumR[v_x:para[var]]$
    para[var] = para[var] + 1$
  }$
  
  
  title_grid = "grid_" + title_y + "_" + title_ratio $
  title_data = title_grid + ".dat" $
  write(title_data, columns) $
  
  plotps title_grid + ".pdf";
  gnuplot;
  set term pdf color
  end;
  
  gnuplot;
  set cbrange[0:4];
  set palette rgb 3,12,0
  set title '@title_grid@'
  set xlabel '@on_x@'
  set ylabel '@on_y@'
  plot '@title_data@' matrix  with image ;
  end;
  plotps_end;
  
}$
  
