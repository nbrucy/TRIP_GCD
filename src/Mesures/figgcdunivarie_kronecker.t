fres=file_open("plot_fig.dat",write);
for scoef=16 to 256 step 16 {

    ftrip="resultat_fig"+str(scoef)+".dat";
    vnumR vdeg_trip, vrtime_trip, vdeg_maple, vrtime_maple;
    read(ftrip, vdeg_trip, vrtime_trip, vdeg_maple, vrtime_maple);
    if (scoef==16) then { file_write(fres, "%g ", vdeg_trip); file_writemsg(fres, "\n"); };
    file_writemsg(fres, "%g ", scoef);
    file_write(fres, "%g ", vrtime_trip/vrtime_maple);
    file_writemsg(fres, "\n");
};
file_close(fres);

plotps "figgcdunivarie_kronecker.pdf";
gnuplot;
set term pdf color
end;

gnuplot;
set cbrange[0:4];
set palette rgb 3,11,6
set title "temps(trip)/temps(maple)"
set xlabel "degre du polynome"
set ylabel "nombre de bits des coefficients"
plot "plot_fig.dat" matrix rowheaders columnheaders with image
end;
plotps_end;