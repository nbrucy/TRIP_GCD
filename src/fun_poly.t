/**
 * 
 * Somes function for polynomials computing
 * 
 * June 2016
 * 
 * Noe Brucy, ENS Rennes,
 * Intern at IMCCE, team ASD
 * Under supervision of Jacques Laskar and Mickael Gastineau
 */


/** 
 * @brief Computes the union of the vars of two series
 * @param A, B are series
 * @param union_var is an id of a tabvar which will contains the result
 * 
 * @return a tabvar, with the vars which are in A or in B.
 */
macro get_union_vars[A, B, union_var]
{
  private _ALL $
  
  t1 = coef_tabvar(A) $
  t2 = coef_tabvar(B) $
  all_var = [t1 : t2] $
  union_var = coef_tabvar(sum j=1 to size(all_var) {return all_var[j]$ }) $
  return union_var $
}$

/** 
 * @brief Computes the infinite norm of a multivariate polynomial.
 * @param P is a polynom 
 * 
 * @return a numeric value equal to the maximum of the magnitude of the coefficients
 */
macro height[P]
{
  return __coef_norm_infinity__(P) $
}$
    
/** 
 * @brief Computes the symetric modulo of a by respect of b.
 * @param a,b are integers 
 * 
 * @return an integer k such as -b/2 < k <= b/2 
 * and a mod b = k mod b
 */
macro symetric_mod[a, b]
{
  return __coef_modulo_coefnum__(a, b) $ 
} $

/** 
 * @brief Computes the symetric modulo of P by respect of b.
 * @param P is a polynom. 
 * @param b is an integer. 
 * 
 * @return a polynom with all coef k verifying -b/2 < k <= b/2 
 * and a mod b = k mod b
 */
macro symetric_mod_poly[P, b]
{
 return __coef_modulo_coefnum__(P, b) $ 
} $

/**
 * @brief returns the leading coefficient of a polynomial in K[x1, .., xk][x],
 * where K is a field (Q or Fp).
 * 
 * @param P is a multivariate polynomial
 * @param x is a variable
 * 
 * @return the leading coefficient (in K[x1, ..., xk])
 */
macro lc[P, x]
{
 return coef_ext(P, (x, puismax(P,x)))$
}$

/** 
 * @brief Computes leading coefficient of P in K[y1, ..., ym, x1, ..., xn] 
 * seen as (K[y1, ..., ym])[x1, ..., xn].
 * (K is a field, K = Q or Fp)
 * 
 * 
 * @param P is a polynom in K[y1, ..., ym, x1, ..., xn]
 * @param vars is a tabvar containing [x1, ..., xn].
 * 
 * @return the leading coefficient (in K[y1, ..., ym])
 */
macro lcoeff_over_poly[P, vars]
{
  private _ALL$
  n = size(vars) $
  if (n > 1) then 
  {
    dimvar rec_vars[1: n - 1] $
    rec_vars := vars[2:n] $
    return %lcoeff_over_poly[%lc[P, [vars[1]]], [rec_vars]]$
  }
  else 
  {
    if (n == 1) then  
    {
      return %lc[P, vars[1]] $
    }
    else
    {
      return P$
    }$
  }$
}$

/**
 * @brief returns the leading coefficient of a polynomial P, 
 * following its order
 * 
 * @param P is a serie over a field K (Q or Fp).
 * 
 * @return the leading coefficient, which is in K.
 */
macro lcoeff[P]
{
  private _ALL$
  vars = coef_tabvar(P) $
  if (size(vars) > 0) then
  {
    return %lcoeff[%lc[P, [vars[1]]]]$
  }
  else 
  {
    return P$
  }$
}$

/** 
 * @brief Computes total degree of the leading coef of P in K[y1, ..., yn, x1, ..., xn] 
 * seen as (K[y1, ..., yn])[x1, ..., xn].
 * (K is a field, K = Q or Fp)
 *  
 * @param P is a polynom in K[y1, ..., yn, x1, ..., xn]
 * @param vars is a tabvar containing [x1, ..., xn].
 * 
 * @return the total (as a vnumR) of P.
 * The total degree is the sum of the degree of the leading monomial.
 * For istance, %total_degree_lc[x^2*y^8*z^9 + 9*x^7*y^8*z^4 + x*y^5*z^12 + z + x + 3, [y, z]] =  8 + 9 = 17.
 */
macro total_degree_lc[P, vars]
{
  private _ALL$
  v = vars $ // Copy it, to access elements.
  
  if (size(vars) > 0) then {
    coef_tab(P, tc, tm, vars)$
    d = sum j= inf(vars, 1) to sup(vars, 1) 
    {
      return puismax(tm[1], v[j])$ 
    }$
    return d$
  }
  else {
    return 0$
  }$
  
}$


/** 
 * @brief Computes the maximum degree of P in K[y1, ..., yn, x1, ..., xn] 
 * seen as (K[y1, ..., yn])[x1, ..., xn].
 * (K is a field, K = Q or Fp)
 *  
 * @param P is a polynom in K[y1, ..., yn, x1, ..., xn]
 * @param vars is a tabvar containing [x1, ..., xn].
 * 
 * @return the maximum degree of P.
 * The maximum degree is the maximum of the degree for each variable
 * For istance, %max_degree[x^2*y^8*z^9 + 9*x^7*y^8*z^4 + x*y^5*z^12 + z + x + 3, [y, z]] = 12.
 */
macro max_degree[P, vars]
{
  private _ALL$
  v = vars $ // Copy it, to access elements.
  
  if (size(vars) > 0) then {
    d = 0 $
    for j= inf(vars, 1) to sup(vars, 1) 
    {
      d = max(d, puismax(P, v[j]))$ 
    }$
    return d$
  }
  else {
    return 0$
  }$
  
}$

/**
 * @brief test wether G | A and G| B
 * 
 * @param A, B, G are series
 * 
 * @return 1 iff G | A and G| B , else 0
 */
macro div_test[A, B, G] 
{
  private _ALL $
  div(A, G, Q, R)$
  
  if (R == 0) then 
  {
    div(B, G, Q, R)$
    if (R == 0) then 
    {
      return 1 $
      stop $
    }$
  }$
        
  return 0 $
}$

/**
 * @brief GCD univariate in K[X] (K = Q or Fp) via Euclide
 * 
 * @params A, B are monovariate series of the same variable over a fiel K.
 * 
 * @return  GCD(A,B) in K[X].
 */
macro UGCD[A, B]
{
  private _ALL$
  return  __coef_gcd__(A, B) $
}$


/**
 * @brief GCD univariate for several polynomial in K[X] (K = Q or Fp) via Euclide
 * 
 * @param t is an array of monovariate series of the same variable over a fiel K.
 * 
 * @return the Gcd of the elements of t in K[X]
 * 
 * Optional args : a other macro for the GCD than UGCD. Then, polynoms could be 
 * multivariate
 */
macro UGCD_tab[t, ...]
{
  private _ALL $
    
  if(size(macro_optargs) == 1) then 
  {
    macro_GCD := macro_optargs[1] $
  } else {
    macro_GCD := UGCD $
  }$
  
  n = size(t) $
  if(n == 0) then {error("UGCD_tab - Void tab")$}
  else
  { 
    
    gcd = t[inf(t, 1)]$
    j = inf(t, 1) + 1  $
    while ((j <= sup(t, 1)) && (gcd != 1)) do
    {
      gcd = %macro_GCD[gcd, t[j]] $
      j = j + 1 $
    }$
    return gcd$
  }$
}$

/**
 * @brief Computes content and primitive part of P in K[x, x1, ..., xn] seen as (K[x])[x1, ..., xn]
 * (K is a field, K = Q or Fp)
 * 
 * 
 * @param P is a polynom in K[x, x1, ..., xn]
 * @param pp is a indentifier for the computed primitive part (which is in K[x, x1, ..., xn])
 * @param cont is a indentifier for the computed content (which is in K[x])
 * @param vars is a tabvar containing [x1, ..., xn].
 * 
 * Optional args : a other macro for the GCD than UGCD. Then, P can be in (K[y1, ..., yn])[x1, ..., xn].
 * 
 */
macro primitive_cont_over_poly[P, pp, cont, vars, ...]
{
  private _ALL$
  coef_tab(P, tc, tm, vars) $
  
  if(size(macro_optargs) == 1)  then 
  {
    macro_GCD := macro_optargs[1] $
  } else {
    macro_GCD := UGCD $
  }$
  

  cont = %UGCD_tab[[tc], macro_GCD] $
  
  
  if (cont == 0) then {pp = 0 $ stop $ } $
    
  div(P, cont, pp, r) $
  
  if(r != 0) then {error("primitive_cont_over_poly : Unexpected non-divisibility, implementation bug")$}$
}$
